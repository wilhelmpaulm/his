<?php

###
### place all the libraries here on the top
include './vendor/autoload.php';
include './lib/config.php';
include './lib/db.php';
include './lib/helper.php';
include './lib/loaders.php';

//include './lib/curly.php';
//include './lib/seats.php';
//include './lib/gcm.php';
//include './lib/social.php';

//header('Access-Control-Allow-Origin: *');
include './router/template/router_header.php';
include './router/_base.php';
include './router/main.php';
include './router/users.php';
include './router/patients.php';
include './router/schedules.php';
include './router/forms.php';
include './router/services.php';
include './router/documents.php';
include './router/matches.php';

include './router/form_braden_risk_assessments.php';
include './router/form_admissions.php';


include './router/template/router_footer.php';
