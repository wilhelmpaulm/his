<div class="row">
    <form action="<?= linkTo("domains/" . $domain["id"]) ?>" method="POST" class="col l12">
        <div class="row">
            <div class="input-field col s12">
                <input id="name" name="name" type="text" required="" value="<?= $domain["name"] ?>" class="validate">
                <label class="active" for="name">name</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="domain" name="domain" type="text" required="" value="<?= $domain["domain"] ?>" class="validate">
                <label class="active" for="domain">domain</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <select name="status">
                    <?php foreach (getOptionList("status_domains") as $ks): ?>
                        <option <?= $ks == $domain["status"] ? "selected=''" : "" ?> value="<?= $ks ?>"><?= $ks ?></option>
                    <?php endforeach; ?>
                </select>
                <label class="active" for="status">status</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light right green" type="submit" name="action">submit
                </button>
            </div>
        </div>
    </form>
</div>