<br/>
<div class="row">
    <div class="input-field col s12">
        <table border="1">
            <thead>
                <tr>
                    <th>name</th>
                    <th>message</th>
                    <th>points</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>sensory perception</td>
                    <td></td>
                  <td><?= $form["sensory_perception"]?></td>
                </tr>
                <tr>
                    <td>nutrition</td>
                    <td></td>
                  <td><?= $form["nutrition"]?></td>
                </tr>
                <tr>
                    <td>activity</td>
                    <td></td>
                  <td><?= $form["activity"]?></td>
                </tr>
                <tr>
                    <td>moisture</td>
                    <td></td>
                  <td><?= $form["moisture"]?></td>
                </tr>
                <tr>
                    <td>mobility</td>
                    <td></td>
                  <td><?= $form["mobility"]?></td>
                </tr>
                <tr>
                    <td>friction and shear</td>
                    <td></td>
                  <td><?= $form["friction_and_shear"]?></td>
                </tr>
                <tr>
                    <td>total</td>
                    <td><?= $form["risk_category"]?></td>
                    <td><?= $form["total_score"]?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
