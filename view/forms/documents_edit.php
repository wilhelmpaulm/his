<div class="row">
    <form action="<?= linkTo("documents/" . $document["id"]) ?>" method="POST" enctype="multipart/form-data" class="col l12">
        <div class="row">
            <div class="input-field col s12">
                <input id="name" name="name" type="text" required="" value="<?= $document["name"] ?>" class="validate">
                <label class="active"  for="name">name</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <select name="type" class="validate">
                    <?php foreach (getOptionList("file_types") as $gen): ?>
                        <option <?= $gen == $document["type"] ? "selected=''" : "" ?> value="<?= $gen ?>"> <?= $gen ?></option>
                    <?php endforeach; ?>
                </select>
                <label class="active"  for="type">type</label>
            </div>
        </div>
        <div class="row">
            <div class="file-field input-field col s12">
                <div class="btn">
                    <span>file (old file:  <?= $document["file"] ?>)</span>
                    <input name="file" required=""  type="file">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="file-field input-field col s12">
                <textarea id="description" name="description" class="materialize-textarea"><?= $document["description"] ?></textarea>
                <label class="active"  for="description">description</label>
            </div>
        </div>
        <div class="row">
            <div class="file-field input-field col s12">
                <textarea id="findings" name="findings" class="materialize-textarea"><?= $document["findings"] ?></textarea>
                <label class="active" for="findings">findings</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light right green" type="submit" name="action">submit
                </button>
            </div>
        </div>
    </form>
</div>