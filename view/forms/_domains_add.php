<div class="row">
    <form action="<?= linkTo("domains") ?>" method="POST" class="col s12 l12">
        <div class="row">
            <div class="input-field col s12">
                <input id="name" name="name" type="text" required="" value="" class="validate">
                <label for="name">name</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="domain" name="domain" type="url" required="" value="" class="validate">
                <label for="domain">domain</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light right green" type="submit" name="action">submit
                </button>
            </div>
        </div>
    </form>
</div>