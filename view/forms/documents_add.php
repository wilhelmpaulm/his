<?php
$patients = selectTable("patients");
?>
<div class="row">
    <form action="<?= linkTo("documents") ?>" method="POST" class="col s12 l12" enctype="multipart/form-data">
        <div class="row">
            <div class="input-field col l12">
                <select name="id_patient" class="validate">
                    <option disabled="" value="">please select patient</option>
                    <?php if (count($patients) > 0 && $patient == null): ?>
                        <?php foreach ($patients as $pat): ?>
                            <option value="<?= $pat["id"] ?>"> <?= $pat["first_name"] . " " . $pat["last_name"] ?></option>
                        <?php endforeach; ?>
                    <?php elseif ($patient != null): ?>
                            <option value="<?= $patient["id"] ?>"> <?= $patient["first_name"] . " " . $patient["last_name"] ?></option>
                    <?php else: ?>
                    <?php endif; ?>
                </select>
                <label for="id_patient">type</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="name" name="name" type="text" required="" value="" class="validate">
                <label for="name">name</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col l12">
                <select name="type" class="validate">
                    <?php foreach (getOptionList("file_types") as $gen): ?>
                        <option value="<?= $gen ?>"> <?= $gen ?></option>
                    <?php endforeach; ?>
                </select>
                <label for="type">type</label>
            </div>
        </div>
        <div class="row">
            <div class="file-field input-field col l12">
                <div class="btn">
                    <span>file</span>
                    <input name="file" required=""  type="file">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="file-field input-field col l8">
                <textarea id="description" name="description" class="materialize-textarea"></textarea>
                <label for="description">description</label>
            </div>
        </div>
        <div class="row">
            <div class="file-field input-field col l8">
                <textarea id="findings" name="findings" class="materialize-textarea"></textarea>
                <label for="findings">findings</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light right green" type="submit" name="action">submit
                </button>
            </div>
        </div>
    </form>
</div>