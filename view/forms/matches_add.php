<div class="row">
    <form action="<?= linkTo("matches") ?>" class="col s12 l12" enctype="multipart/form-data">
        <input name="id_patient" value="<?= $patient["id"]?>" type="hidden">
        <input name="id_user" value="<?= user("id")?>" type="hidden">
        <div class="row">
            <div class="file-field input-field col s12">
                <textarea id="query" name="query" class="materialize-textarea"></textarea>
                <label for="query">query</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light right green" type="submit" name="action">submit
                </button>
            </div>
        </div>
    </form>
</div>