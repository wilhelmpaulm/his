<div class="row">
    <form action="<?= linkTo("schedules") ?>" method="POST" class="col s12 l12" enctype="multipart/form-data">
        <input name="id_patient" value="<?= $patient["id"] ?>" type="hidden">
        <input name="id_user" value="<?= $user["id"] ?>" type="hidden">

        <div class="row">
            <div class="input-field col s12">
                <input id="day" required="" name="day" type="date" class="datepicker">
                <label class="active" for="day">day</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <select name="time_start" class="select-dropdown">
                    <option disabled="">please select date then time</option>
                    <?php foreach (getTimeList() as $key => $val): ?>
                    <option  value="<?= $val ?>"><?= $val ?></option>
                    <?php endforeach; ?>
                </select>
                <label class="active" >Time start</label>
            </div>
        </div>
        <div class="row">
            <div class="file-field input-field col s12">
                <textarea id="note" name="note" class="materialize-textarea"></textarea>
                <label for="note">note</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light right green" type="submit" name="action">submit
                </button>
            </div>
        </div>
    </form>
</div>
<script>
    $("document").ready(function () {
        $('input[name="day"]').change(function () {
            var day = $(this).val();
            var date = new Date(day);
            var day_index = date.getDay();
            var id_user = $('[name="id_user"]').val();

//            $("[name='time_start']").children().remove();
//            var request = $.ajax({
//                method: "GET",
//                url: "<?= linkTo("schedules/view/form/") ?> " + day_index,
//                data: {id_user: id_user},
//                dataType: "html"
//            });
//            request.done(function (data) {
//                $("[name='time_start']").children().remove();
//                $("[name='time_start']").append(data);
//                $('select').material_select('destroy');
//                $('select').material_select();
//                alert("success");
//                console.log(data);
//            });
//            request.fail(function (data) {
//                alert("fail");
//                console.log(data);
//            });

        });
    });
</script>