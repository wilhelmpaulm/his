
<div class="row">
    <br/>
    <form action="<?= linkTo("form_braden_risk_assessments/" . $form_braden_risk_assessment["id"]) ?>" method="POST" class="col l12">
        <div class="row">
            <div class="input-field col s12">
                <select name="sensory_perception">
                    <?php foreach (getOptionList("health_history", "sensory_perception") as $k => $ks): ?>
                        <option <?= $k == $form_braden_risk_assessment["sensory_perception"] ? "selected=''" : "" ?> value="<?= $k ?>"><?= $ks ?></option>
                    <?php endforeach; ?>
                </select>
                <label class="active" for="sensory_perception">sensory_perception</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <select name="mobility">
                    <?php foreach (getOptionList("health_history", "mobility") as $k => $ks): ?>
                        <option <?= $k == $form_braden_risk_assessment["mobility"] ? "selected=''" : "" ?> value="<?= $k ?>"><?= $ks ?></option>
                    <?php endforeach; ?>
                </select>
                <label class="active" for="mobility">mobility</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <select name="moisture">
                    <?php foreach (getOptionList("health_history", "moisture") as $k => $ks): ?>
                        <option <?= $k == $form_braden_risk_assessment["moisture"] ? "selected=''" : "" ?> value="<?= $k ?>"><?= $ks ?></option>
                    <?php endforeach; ?>
                </select>
                <label class="active" for="moisture">moisture</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <select name="activity">
                    <?php foreach (getOptionList("health_history", "activity") as $k => $ks): ?>
                        <option <?= $k == $form_braden_risk_assessment["activity"] ? "selected=''" : "" ?> value="<?= $k ?>"><?= $ks ?></option>
                    <?php endforeach; ?>
                </select>
                <label class="active" for="activity">activity</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <select name="nutrition">
                    <?php foreach (getOptionList("health_history", "nutrition") as $k => $ks): ?>
                        <option <?= $k == $form_braden_risk_assessment["nutrition"] ? "selected=''" : "" ?> value="<?= $k ?>"><?= $ks ?></option>
                    <?php endforeach; ?>
                </select>
                <label class="active" for="nutrition">nutrition</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <select name="friction_and_shear">
                    <?php foreach (getOptionList("health_history", "friction_and_shear") as $k => $ks): ?>
                        <option <?= $k == $form_braden_risk_assessment["friction_and_shear"] ? "selected=''" : "" ?> value="<?= $k ?>"><?= $ks ?></option>
                    <?php endforeach; ?>
                </select>
                <label class="active" for="friction_and_shear">friction_and_shear</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light right green" type="submit" name="action">submit
                </button>
            </div>
        </div>
    </form>
</div>