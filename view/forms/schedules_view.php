<br/>
<div class="row">
    <div class="input-field col s12">
        <table class="responsive-table bordered highlight striped">
            <thead>
                <tr>
                    <th>date created</th>
                    <th>status</th>
                    <th>day</th>
                    <th>time</th>
                    <th>patient</th>
                    <th>user</th>
                    <th>
                        <!--<a href="#modal_documents_add"  class="right btn green waves-effect waves-light  modal-trigger">add</a>-->

                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($schedules as $sch): ?>
                    <tr class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="<?= $sch["note"] ?>">
                        <td><?= $sch["date_created"] ?></td>
                        <td><?= $sch["status"] ?></td>
                        <td class="name"> <?= $sch["day"] ?></td>
                        <td><?= $sch["time_start"] ?></td>
                        <?php $pat = seekTable("patients", $sch["id_patient"]) ?>
                        <td><?= $pat["last_name"] . ", " . $pat["first_name"] ?></td>

                        <?php $use = seekTable("users", $sch["id_user"]) ?>
                        <td><?= $use["last_name"] . ", " . $use["first_name"] ?></td>

                        <td>
                            <div class="right">
                                <a href="<?= linkTo("schedules/" . $sch["id"] . "/delete") ?>"class="btn red waves-effect waves-light ">delete</a>
                                <a href="<?= linkTo("schedules/" . $sch["id"] . "/done") ?>"class="btn green waves-effect waves-light ">done</a>
                                <!--<a href="#modal_documents_edit" data-id="<?= $sch["id"] ?>" class="btn light-blue waves-effect waves-light ">edit</a>-->
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
