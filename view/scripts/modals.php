<script>
    $("document").ready(function () {
<?php
$modals = getTableFormList("form_admissions");
array_push($modals, "documents");
?>
<?php foreach ($modals as $fal): ?>
    <?php if (!empty($$fal)): ?>
        <?php $sam = $$fal ?>
                $("[href='#modal_<?= $fal ?>_edit']").on("click", function () {
                    $("#modal_<?= $fal ?>_edit_form").children().remove();
                    $("#modal_<?= $fal ?>_edit_form").append("<div class='center'><span class='fa fa-5x fa-spin fa-spinner fa-pulse light-blue-text'></span></div>");
                    var id = $(this).data("id");
                    $('#modal_<?= $fal ?>_edit').openModal();
                    var request = $.ajax({
                        method: "POST",
                        url: "<?= linkTo($fal . "/edit/form") ?>",
                        data: {id: id},
                        dataType: "html"
                    });
                    request.done(function (data) {
                        $("#modal_<?= $fal ?>_edit_form").children().remove();
                        $("#modal_<?= $fal ?>_edit_form").append(data);
                        $('select').material_select('destroy');
                        $('select').material_select();
                        console.log(data);
                    });
                    request.fail(function (data) {
                        console.log(data);
                    });
                });
                $("[href='#modal_<?= $fal ?>_view']").on("click", function () {
                    $("#modal_<?= $fal ?>_view_form").children().remove();
                    $("#modal_<?= $fal ?>_view_form").append("<div class='center'><span class='fa fa-5x fa-spin fa-spinner fa-pulse light-blue-text'></span></div>");
                    var id = $(this).data("id");
                    $('#modal_<?= $fal ?>_view').openModal();
                    var request = $.ajax({
                        method: "POST",
                        url: "<?= linkTo($fal . "/view/form") ?>",
                        data: {id: id},
                        dataType: "html"
                    });
                    request.done(function (data) {
                        $("#modal_<?= $fal ?>_view_form").children().remove();
                        $("#modal_<?= $fal ?>_view_form").append(data);
                        $('select').material_select('destroy');
                        $('select').material_select();
                        console.log(data);
                    });
                    request.fail(function (data) {
                        console.log(data);
                    });
                });
    <?php endif; ?>
<?php endforeach; ?>

        $("[href='#modal_matches_add']").on("click", function () {
            $("#modal_matches_add_form").children().remove();
            $("#modal_matches_add_form").append("<div class='center'><span class='fa fa-5x fa-spin fa-spinner fa-pulse light-blue-text'></span></div>");
            var id = $(this).data("id");
            $('#modal_matches_add').openModal();
            var request = $.ajax({
                method: "POST",
                url: "<?= linkTo("matches/add/form") ?>",
                data: {id: id},
                dataType: "html"
            });
            request.done(function (data) {
                $("#modal_matches_add_form").children().remove();
                $("#modal_matches_add_form").append(data);
                $('select').material_select('destroy');
                $('select').material_select();
                console.log(data);
            });
            request.fail(function (data) {
                console.log(data);
            });
        });

        $("[href='#modal_schedules_add']").on("click", function () {
            $("#modal_schedules_add_form").children().remove();
            $("#modal_schedules_add_form").append("<div class='center'><span class='fa fa-5x fa-spin fa-spinner fa-pulse light-blue-text'></span></div>");
            var id_patient = $(this).data("id_patient");
            var id_user = $(this).data("id_user");
            $('#modal_schedules_add').openModal();
            var request = $.ajax({
                method: "GET",
                url: "<?= linkTo("schedules/add/form") ?>",
                data: {id_patient: id_patient, id_user: id_user},
                dataType: "html"
            });
            request.done(function (data) {
                $("#modal_schedules_add_form").children().remove();
                $("#modal_schedules_add_form").append(data);
                $('select').material_select('destroy');
                $('select').material_select();
                $('.datepicker.year').pickadate({
                    closeOnSelect: false,
                    selectMonths: false,
                     selectYears: 150// Creates a dropdown to control month
                });
                $('.datepicker').pickadate({
                    closeOnSelect: true,
                    format: 'yyyy-mm-dd',
                    formatSubmit: 'yyyy-mm-dd',
                    selectMonths: true,
                     selectYears: 150// Creates a dropdown to control month
                });
                console.log(data);
            });
            request.fail(function (data) {
                console.log(data);
            });
        });
        
        $("[href='#modal_schedules_view']").on("click", function () {
            $("#modal_schedules_view_form").children().remove();
            $("#modal_schedules_view_form").append("<div class='center'><span class='fa fa-5x fa-spin fa-spinner fa-pulse light-blue-text'></span></div>");
            var id_user = $(this).data("id_user");
            $('#modal_schedules_view').openModal();
            var request = $.ajax({
                method: "GET",
                url: "<?= linkTo("schedules/view/form") ?>",
                data: {id_user: id_user},
                dataType: "html"
            });
            request.done(function (data) {
                $("#modal_schedules_view_form").children().remove();
                $("#modal_schedules_view_form").append(data);
                console.log(data);
            });
            request.fail(function (data) {
                console.log(data);
            });
        });

    });
</script>