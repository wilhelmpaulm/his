<?php include linkPage("template/header"); ?>
<body>
    <?php include linkPage("nav/header"); ?>
    <div class="container">
        <div class="section">
            <div id="404" class="row">
                <div class="col s12 l12">
                    <div class="card-panel  z-depth-2 center">
                        <span class="card-title red-text lighten-1">ERROR 404</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php // include linkPage("nav/footer"); ?>
</body>
<?php include linkPage("template/footer"); ?>