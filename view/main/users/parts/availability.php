<div class="row" id="patients">
    <div class="row " >
        <div class="col s9">
            <nav>
                <div class="nav-wrapper red lighten-1">
                    <form>
                        <div class="input-field">
                            <input id="search" class="search"  type="search" required>
                            <label for="search"><i><span class="fa fa-search"></span></i></label>
                            <i class="material-icons fa fa-times"></i>
                        </div>
                    </form>
                </div>  
            </nav>  
        </div>
        <div class="col s3">
            <nav class="waves-effect waves-light waves-ripple sort" style="background-color: #EF5350" data-sort="name">
                <a class="red lighten-1 btn-large block"><span class="fa fa-sort"></span> sort</a>
            </nav>
        </div>
    </div>
    <div class="card-panel white black-text">
        <div class="" style="height: 100%">
            <span class="card-title black-text">Availability</span>
            <form action="<?= linkTo("users/" . user("id") . "/availability") ?>" method="POST">
                <table class="table  responsive-table bordered striped">
                    <thead>
                        <tr>
                            <th width="25%">day</th>
                            <th>time start</th>
                            <th>time end</th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        <?php foreach ($availabilities as $key => $ava): ?>
                            <tr>
                                <td class="name">
                                    <input name="id_user[]" type="hidden" value="<?= $ava["id_user"] ?>">
                                    <input name="day[]" type="hidden" value="<?= $ava["day"] ?>">
                                    <input name="id[]" type="hidden" value="<?= $ava["id"] ?>">
                                    <?= $ava["day"] ?>
                                </td>
                                <td>
                                    <select name="time_start[]" class="select-dropdown">
                                        <?php foreach (getTimeList() as $key => $tim): ?>
                                            <option <?= $ava["time_start"] == $tim . "" ? "selected=''" : "" ?> value="<?= $tim ?>"><?= $tim ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label class="active" >Time start</label>
                                </td>
                                <td>
                                    <select name="time_end[]" class="select-dropdown">
                                        <?php foreach (getTimeList() as $key => $tim): ?>
                                            <option <?= $ava["time_end"] == $tim . "" ? "selected=''" : "" ?>  value="<?= $tim ?>"><?= $tim ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <label class="active" >Time end</label>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td class="name"></td>
                            <td>
                            </td>
                            <td>
                                <div class="right">
                                    <button class="waves-effect waves-light btn"><span class="fa fa-check"></span> set</button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>

        </div>
    </div>
</div>