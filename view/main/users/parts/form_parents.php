<div class="card white black-text">
    <div class="card-content">
        <span class="card-title black-text">Family Medical History</span>
        <div class="row">
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s6">
                        <textarea class="materialize-textarea"></textarea>
                        <label for="email">Mother</label>
                    </div>
                    <div class="input-field col s6">
                        <textarea class="materialize-textarea"></textarea>
                        <label for="email">Father</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <textarea class="materialize-textarea"></textarea>
                        <label for="email">Others</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
