<div class="row" id="patients">
    <div class="row " >
        <div class="col s9">
            <nav>
                <div class="nav-wrapper red lighten-1">
                    <form>
                        <div class="input-field">
                            <input id="search" class="search"  type="search" required>
                            <label for="search"><i><span class="fa fa-search"></span></i></label>
                            <i class="material-icons fa fa-times"></i>
                        </div>
                    </form>
                </div>  
            </nav>  
        </div>
        <div class="col s3">
            <nav class="waves-effect waves-light waves-ripple sort" style="background-color: #EF5350" data-sort="name">
                <a class="red lighten-1 btn-large block"><span class="fa fa-sort"></span> sort</a>
            </nav>
        </div>
    </div>
    <div class="card-panel white black-text">
        <div class="" style="height: 100%">
            <span class="card-title black-text">Services</span>
            <table class="table  responsive-table bordered striped">
                <thead>
                    <tr>
                        <th width="25%">Service</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="list">
                    <tr>
                        <td class="name">Availability</td>
                        <td>availability of the user for schedules</td>
                        <td>
                            <div class="right">
                                <a class="waves-effect waves-light btn"><span class="fa fa-list"></span> View</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">Schedules</td>
                        <td>user schedules</td>
                        <td>
                            <div class="right">
                                <a class="waves-effect waves-light btn"><span class="fa fa-list"></span> history</a>
                                <a class="waves-effect waves-light btn"> <span class="fa fa-plus"></span> add</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">Reports</td>
                        <td>user reports</td>
                        <td>
                            <div class="right">
                                <a class='dropdown-button waves-effect waves-light btn block' href='#' data-activates='dropdown3'><span class="fa fa-dashboard"></span> types of reports</a>
                                <ul id='dropdown3' class='dropdown-content'>
                                    <li><a href="#!">profile summary</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#!">turnover rate</a></li>
                                    <li><a href="#!">operations</a></li>
                                    <li><a href="#!">documents</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>