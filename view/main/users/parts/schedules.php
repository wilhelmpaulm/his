<div class="" id="patients">
    <div class="row " >
        <div class="col s9">
            <nav>
                <div class="nav-wrapper red lighten-1">
                    <form>
                        <div class="input-field">
                            <input id="search" class="search"  type="search" required>
                            <label for="search"><i><span class="fa fa-search"></span></i></label>
                            <i class="material-icons fa fa-times"></i>
                        </div>
                    </form>
                </div>  
            </nav>  
        </div>
        <div class="col s3">
            <nav class="waves-effect waves-light waves-ripple sort" style="background-color: #EF5350" data-sort="name">
                <a class="red lighten-1 btn-large block"><span class="fa fa-sort"></span> sort</a>
            </nav>
        </div>
    </div>
    <div class="card white black-text">
        <div class="card-content">
            <span class="card-title black-text">Schedules</span>
            <table class="table  responsive-table bordered striped">
                <thead>
                    <tr>
                        <th>date created</th>
                        <th>status</th>
                        <th>day</th>
                        <th>time</th>
                        <th>patient</th>
                        <th>user</th>
                        <th>
                            <!--<a href="#modal_documents_add"  class="right btn green waves-effect waves-light  modal-trigger">add</a>-->

                        </th>
                    </tr>
                </thead>
                <tbody class="list">
                    <?php foreach ($schedules as $sch): ?>
                        <tr class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="<?= $sch["note"] ?>">
                            <td><?= $sch["date_created"] ?></td>
                            <td><?= $sch["status"] ?></td>
                            <td class="name"> <?= $sch["day"] ?></td>
                            <td><?= $sch["time_start"] ?></td>
                            <?php $pat = seekTable("patients", $sch["id_patient"]) ?>
                            <td><?= $pat["last_name"] . ", " . $pat["first_name"] ?></td>

                            <?php $use = seekTable("users", $sch["id_user"]) ?>
                            <td><?= $use["last_name"] . ", " . $use["first_name"] ?></td>

                            <td>
                                <div class="right">
                                    <a href="<?= linkTo("schedules/" . $sch["id"] . "/delete") ?>"class="btn red waves-effect waves-light ">delete</a>
                                    <a href="<?= linkTo("schedules/" . $sch["id"] . "/done") ?>"class="btn green waves-effect waves-light ">done</a>
                                    <!--<a href="#modal_documents_edit" data-id="<?= $sch["id"] ?>" class="btn light-blue waves-effect waves-light ">edit</a>-->
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php include linkPage("modals/documents_add") ?>
<?php include linkPage("modals/documents_edit") ?>
<?php include linkPage("scripts/modals") ?>