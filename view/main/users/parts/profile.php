<div class="row">
    <div class="col s12 l3">
        <div class="card medium">
            <div class="card-image">
                 <img  src="/public/<?= user("image") ?>" style="max-height: 16em; height: 16em">
                <span class="card-title name block" style="background: rgba(0,0,0,.5); font-size: larger"><?= user("first_name") . " " . user("middle_name") . " " . user("last_name") ?></span>
            </div>
            <div class="card-content center">
                <p><?= user("type") ?> </p>
                <hr/>
                <p><?= user("specialization") ?> </p>
            </div>
        </div>
    </div>
    <div class="col s12 l9">
        <div class="card card-panel " style="max-height: 400px; min-height: 400px; overflow-y: scroll">
            <div class="">
                <div class="card-content">
                    <div class="ct-chart2">

                    </div>
                    <div class="ct-chart1">

                    </div>
                    <div class="ct-chart3">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    new Chartist.Line('.ct-chart1', {
        labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
        series: [
            [12, 9, 7, 8, 5],
            [2, 1, 3.5, 7, 3],
            [1, 3, 4, 5, 6]
        ]
    }, {
        fullWidth: true,
        chartPadding: {
            right: 40
        }
    });
    new Chartist.Bar('.ct-chart2', {
        labels: ['XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL'],
        series: [20, 60, 120, 200, 180, 20, 10]
    }, {
        distributeSeries: true
    });
    new Chartist.Line('.ct-chart3', {
        labels: [1, 2, 3, 4, 5, 6, 7, 8],
        series: [
            [1, 2, 3, 1, -2, 0, 1, 0],
            [-2, -1, -2, -1, -2.5, -1, -2, -1],
            [0, 0, 0, 1, 2, 2.5, 2, 1],
            [2.5, 2, 1, 0.5, 1, 0.5, -1, -2.5]
        ]
    }, {
        high: 3,
        low: -3,
        showArea: true,
        showLine: false,
        showPoint: false,
        fullWidth: true,
        axisX: {
            showLabel: false,
            showGrid: false
        }
    });
</script>