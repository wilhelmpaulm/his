<?php include linkPage("template/header"); ?>
<body>
    <?php include linkPage("nav/header"); ?>

    <div class="container">
        <div class="section" id="patients">
            <div class="row">
                <div class="col s9">
                    <nav>
                        <div class="nav-wrapper red lighten-1">
                            <form>
                                <div class="input-field">
                                    <input id="search" class="search"  type="search" required>
                                    <label for="search"><i><span class="fa fa-search"></span></i></label>
                                    <i class="material-icons fa fa-times"></i>
                                </div>
                            </form>
                        </div>  
                    </nav>  
                </div>
                <div class="col s3">
                    <nav class="waves-effect waves-light waves-ripple sort" style="background-color: #EF5350" data-sort="name">
                        <a class="red lighten-1 btn-large block"><span class="fa fa-sort"></span> sort</a>
                    </nav>
                </div>
            </div>
            <div class="row list">
                <?php foreach ($users as $use): ?>
                    <div class="col s12 m4 l3">
                        <div class="card white">
                            <div class="card-image">
                                <img  src="/public/<?= $use["image"] ?>" style="max-height: 16em; height: 16em">
                                <span class="card-title name block" style="background: rgba(0,0,0,.5); font-size: larger"><?= $use["first_name"] . " " . $use["middle_name"] . " " . $use["last_name"] ?></span>
                            </div>
                            <div class="card-content center">
                                <p><?= $use["type"] ?> </p>
                                <hr/>
                                <p> <?= $use["specialization"] ?> </p>
                            </div>
                            <div class="card-action">
                                <a href="<?= linkTo("users/".$use["id"]."/availability")?>">Availability</a>
                                <a href="<?= linkTo("users/".$use["id"])?>">Profile</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
    <script>
        var options = {
            valueNames: ['name']
        };

        var userList = new List('patients', options);
    </script>
    <?php // include linkPage("nav/footer"); ?>
</body>
<?php include linkPage("template/footer"); ?>