<?php include linkPage("template/header"); ?>
<body>
    <?php include linkPage("nav/header"); ?>

    <div class="container">
        <div class="section">
            <?php include linkPage("main/users/parts/profile"); ?>
            <div class="row">
                <div class="col s12 m12">
                    <?php // include linkPage("main/users/parts/services"); ?>
                </div>
            </div>

        </div>
    </div>
    <script>
        var options = {
            valueNames: ['name']
        };

        var userList = new List('patients', options);
    </script>

    <?php // include linkPage("nav/footer"); ?>
</body>
<?php include linkPage("template/footer"); ?>