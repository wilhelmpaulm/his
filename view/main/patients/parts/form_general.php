<div class="card-panel white black-text">
    <div class="">
        <span class="card-title black-text">Patient Social History</span>
        <div class="row">
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s3">
                        <select name="present_smoker">
                            <option>yes</option>
                            <option>no</option>
                        </select>
                        <label>Present smoker?</label>
                    </div>
                    <div class="input-field col s3">
                        <input id="password" type="text" value="0" class="validate">
                        <label for="email">Number of sticks per day?</label>
                    </div>
                      <div class="input-field col s3">
                        <input id="password" type="number" value="0" class="validate">
                        <label for="email">Average duration per stick (minutes)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s3">
                        <select>
                            <option value="1">NO</option>
                            <option value="2">YES</option>
                        </select>
                        <label for="email">Previous Smoker?</label>
                    </div>
                    <div class="input-field col s3">
                        <input id="password" type="number" value="0" class="validate">
                        <label for="email">Year quit smoking?</label>
                    </div>
                    <div class="input-field col s3">
                        <input id="password" type="number" value="0" class="validate">
                        <label for="email">Number of sticks per day?</label>
                    </div>
                    <div class="input-field col s3">
                        <input id="password" type="number" value="0" class="validate">
                        <label for="email">Average duration per stick (minutes)</label>
                    </div>
                </div>
                  <div class="row">
                    <div class="input-field col s6">
                        <textarea class="materialize-textarea"></textarea>
                        <label for="email">Smoking remarks</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s3">
                        <select>
                            <option value="1">NO</option>
                            <option value="2">YES</option>
                        </select>
                        <label for="email">Alcoholic beverage drinker?</label>
                    </div>
                    <div class="input-field col s3">
                        <select>
                            <option value="1">NO</option>
                            <option value="2">YES</option>
                        </select>
                        <label for="email">Previous drinker?</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <textarea class="materialize-textarea"></textarea>
                        <label for="email">Drinking remarks</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
