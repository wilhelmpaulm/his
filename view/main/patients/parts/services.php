<div class="" id="patients">
    <div class="row " >
        <div class="col s9">
            <nav>
                <div class="nav-wrapper red lighten-1">
                    <form>
                        <div class="input-field">
                            <input id="search" class="search"  type="search" required>
                            <label for="search"><i><span class="fa fa-search"></span></i></label>
                            <i class="material-icons fa fa-times"></i>
                        </div>
                    </form>
                </div>  
            </nav>  
        </div>
        <div class="col s3">
            <nav class="waves-effect waves-light waves-ripple sort" style="background-color: #EF5350" data-sort="name">
                <a class="red lighten-1 btn-large block"><span class="fa fa-sort"></span> sort</a>
            </nav>
        </div>
    </div>
    <div class="card white black-text">
        <div class="card-content">
            <span class="card-title black-text">Services</span>
            <table class="table  responsive-table bordered striped">
                <thead>
                    <tr>
                        <th width="25%">Service</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="list">
                    <tr>
                        <td class="name">Admission Form</td>
                        <td>initial form filled up at entry</td>
                        <td>
                            <div class="right">
                                <a href="<?= linkTo("patients/".$patient["id"]."/form_admissions") ?>" class="waves-effect waves-light btn blue">view</a>
<!--                                <form style="display: inline" action="<?= linkTo("form_admissions") ?>" method="POST">
                                    <input type="hidden"  name="id_patient" value="<?= $patient["id"] ?>">
                                    <button class="waves-effect waves-light btn green">add</button>
                                </form>-->
                                <!--                                <a href="" class="waves-effect waves-light btn green">add</a>-->
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">Documents</td>
                        <td>documents uploaded by medtechs and staff members</td>
                        <td>
                            <div class="right">
                                <a href="<?= linkTo("patients/".$patient["id"]."/documents") ?>" class="waves-effect waves-light btn blue">view</a>
                               
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>