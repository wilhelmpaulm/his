<div class="" id="patients">
    <div class="row " >
        <div class="col s9">
            <nav>
                <div class="nav-wrapper red lighten-1">
                    <form>
                        <div class="input-field">
                            <input id="search" class="search"  type="search" required>
                            <label for="search"><i><span class="fa fa-search"></span></i></label>
                            <i class="material-icons fa fa-times"></i>
                        </div>
                    </form>
                </div>  
            </nav>  
        </div>
        <div class="col s3">
            <nav class="waves-effect waves-light waves-ripple sort" style="background-color: #EF5350" data-sort="name">
                <a class="red lighten-1 btn-large block"><span class="fa fa-sort"></span> sort</a>
            </nav>
        </div>
    </div>
    <div class="card white black-text">
        <div class="card-content">
            <span class="card-title black-text">Form Admission</span>
            <table class="table  responsive-table bordered striped">
                <thead>
                    <tr>
                        <th>status</th>
                        <th width="50%">form</th>
                        <th>date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="list">
                    <?php foreach (getTableFormList("form_admissions") as $fal): ?>
                        <?php $sam = $$fal ?>
                        <tr>
                            <td><?= $sam["date_updated"] != null ? "filled" : "unfilled"; ?></td>
                            <td class="name"><?= str_replace("_", " ", $fal) ?></td>
                            <td><?= $sam["date_created"] ?></td>
                            <td>
                                <div class="right">
                                    <a href="#modal_<?= $fal ?>_view" data-id="<?= $sam["id"] ?>" class="btn light-blue waves-effect waves-light ">view</a>
                                    <a href="#modal_<?= $fal ?>_edit" data-id="<?= $sam["id"] ?>" class="btn orange waves-effect waves-light ">edit</a>
                                    <!--<a href="<?= linkTo("form_admissions") ?>" class="waves-effect waves-light btn blue">edit</a>-->
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php include linkPage("modals/form_braden_risk_assessments_edit") ?>
<?php include linkPage("modals/form_braden_risk_assessments_view") ?>
<?php include linkPage("scripts/modals") ?>
