<div class="" id="patients">
    <div class="row " >
        <div class="col s9">
            <nav>
                <div class="nav-wrapper red lighten-1">
                    <form>
                        <div class="input-field">
                            <input id="search" class="search"  type="search" required>
                            <label for="search"><i><span class="fa fa-search"></span></i></label>
                            <i class="material-icons fa fa-times"></i>
                        </div>
                    </form>
                </div>  
            </nav>  
        </div>
        <div class="col s3">
            <nav class="waves-effect waves-light waves-ripple sort" style="background-color: #EF5350" data-sort="name">
                <a class="red lighten-1 btn-large block"><span class="fa fa-sort"></span> sort</a>
            </nav>
        </div>
    </div>
    <div class="card white black-text">
        <div class="card-content">
            <span class="card-title black-text">Documents</span>
            <table class="table  responsive-table bordered striped">
                <thead>
                    <tr>
                        <th>date created</th>
                        <th>type</th>
                        <th>name</th>
                        <?php if (!$patient): ?>
                            <th>patient</th>
                        <?php endif; ?>
                        <th>user</th>
                        <th>file</th>
                        <th>
                            <a href="#modal_documents_add"  class="right btn green waves-effect waves-light  modal-trigger">add</a>

                        </th>
                    </tr>
                </thead>
                <tbody class="list">
                    <?php foreach ($documents as $doc): ?>
                        <tr>
                            <td><?= $doc["date_created"] ?></td>
                            <td><?= $doc["type"] ?></td>
                            <td class="name tooltipped" data-position="bottom" data-delay="50" data-tooltip="I am tooltip"> <?= $doc["name"] ?></td>
                            <?php if (!$patient): ?>
                                <?php $pat = seekTable("patients", $doc["id_patient"]) ?>
                                <td><?= $pat["last_name"] . ", " . $pat["first_name"] ?></td>
                            <?php endif; ?>

                            <?php $use = seekTable("users", $doc["id_user"]) ?>
                            <td><?= $use["last_name"] . ", " . $use["first_name"] ?></td>
                            <td>
                                <a href="<?= linkPublic("files/" . $doc["file"]) ?>" target="_blank" class="btn waves-ripple waves-light">
                                    <span class="fa fa-file"></span>
                                </a>
                            </td>
                            <td>
                                <div class="right">
                                    <a href="<?= linkTo("documents/" . $doc["id"] . "/delete") ?>"class="btn red waves-effect waves-light ">delete</a>
                                    <a href="#modal_documents_edit" data-id="<?= $doc["id"] ?>" class="btn light-blue waves-effect waves-light ">edit</a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php include linkPage("modals/documents_add") ?>
<?php include linkPage("modals/documents_edit") ?>
<?php include linkPage("scripts/modals") ?>