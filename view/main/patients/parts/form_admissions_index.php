<div class="" id="patients">
    <div class="row " >
        <div class="col s9">
            <nav>
                <div class="nav-wrapper red lighten-1">
                    <form>
                        <div class="input-field">
                            <input id="search" class="search"  type="search" required>
                            <label for="search"><i><span class="fa fa-search"></span></i></label>
                            <i class="material-icons fa fa-times"></i>
                        </div>
                    </form>
                </div>  
            </nav>  
        </div>
        <div class="col s3">
            <nav class="waves-effect waves-light waves-ripple sort" style="background-color: #EF5350" data-sort="name">
                <a class="red lighten-1 btn-large block"><span class="fa fa-sort"></span> sort</a>
            </nav>
        </div>
    </div>
    <div class="card white black-text">
        <div class="card-content">
            <span class="card-title black-text">Admission Forms</span>
            <table class="table  responsive-table bordered striped">
                <thead>
                    <tr>
                        <th>date created</th>
                        <?php if (!$patient): ?>
                            <th>patient</th>
                        <?php endif; ?>
                        <th>doctor</th>
                        <th>
                            <span>
                                <form style="display: inline" class="right" action="<?= linkTo("form_admissions") ?>" method="POST">
                                    <input type="hidden"  name="id_patient" value="<?= $patient["id"] ?>"/>
                                    <button class="waves-effect waves-light btn green">add</button>
                                </form>
                            </span>
                        </th>
                    </tr>
                </thead>
                <tbody class="list">
                    <?php foreach ($form_admissions as $fad): ?>
                        <tr>
                            <td><?= $fad["date_created"] ?></td>
                             <?php if (!$patient): ?>
                                <?php $pat = seekTable("patients", $fad["id_patient"]) ?>
                                <td><?= $pat["last_name"] . ", " . $pat["first_name"] ?></td>
                            <?php endif; ?>
                            <?php $use = seekTable("users", $fad["id_user"]) ?>
                            <td><?= $use["last_name"] . ", " . $use["first_name"] ?></td>
                            <td>
                                <div class="right">
                                    <a href="<?= linkTo("patients/" . $patient["id"] . "/form_admissions/" . $fad["id"]) ?>"  class="btn light-blue waves-effect waves-light ">view</a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
