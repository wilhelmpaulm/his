<div class="card-panel white black-text hoverable">
    <div class="">
        <span class="card-title black-text">Add New Schedule</span>
        <div class="row">
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s4">
                        <input  class="datepicker" type="date" />
                        <label>Date</label>
                    </div>
                    <div class="input-field col s4">
                        <select name="" class="block select-dropdown">
                            <?php foreach (getTimeList() as $key => $tim): ?>
                                <option value="<?= $tim ?>"><?= $tim ?></option>
                            <?php endforeach; ?>
                        </select>
                        <label>Time</label>
                    </div>
                    <div class="input-field col s4">
                        <button class="btn block waves-ripple waves-effect">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="section" id="patients">
    <div class="row">
        <div class="col s9">
            <nav>
                <div class="nav-wrapper red lighten-1">
                    <form>
                        <div class="input-field hoverable">
                            <input id="search" class="search"  type="search" required>
                            <label for="search"><i><span class="fa fa-search"></span></i></label>
                            <i class="material-icons fa fa-times"></i>
                        </div>
                    </form>
                </div>  
            </nav>  
        </div>
        <div class="col s3">
            <nav class="waves-effect waves-light waves-ripple sort hoverable" style="background-color: #EF5350" data-sort="name">
                <a class="red lighten-1 btn-large block"><span class="fa fa-sort"></span> sort</a>
            </nav>
        </div>
    </div>
    <div class="row list">
        <div class="col s12 l3 m4 ">
            <div class="card small hoverable">
                <div class="card-image">
                    <img  src="/public/patients/01.png">
                    <span class="card-title name">John Doe</span>
                </div>
                <div class="card-content center">
                    <p>5'7 ft | 82 Kgs</p>
                    <hr/>
                    <p>No pending schedule</p>
                </div>
                <div class="card-action">
                    <a class="right" href="#">Add Schedule</a>
                </div>
            </div>
        </div>
        <div class="col s12 l3 m4 ">
            <div class="card small hoverable">
                <div class="card-image">
                    <img src="/public/patients/02.png">
                    <span class="card-title name">Magan Young</span>
                </div>
                <div class="card-content center">
                    <p>5'7 ft | 82 Kgs</p>
                    <hr/>
                    <p>No pending schedule</p>
                </div>
                <div class="card-action">
                    <a class="right" href="#">Add Schedule</a>
                </div>
            </div>
        </div>
        <div class="col s12 l3 m4 ">
            <div class="card small hoverable">
                <div class="card-image">
                    <img src="/public/patients/04.png">
                    <span class="card-title name">Bang Ding</span>
                </div>
                <div class="card-content center">
                    <p>5'7 ft | 82 Kgs</p>
                    <hr/>
                    <p>No pending schedule</p>
                </div>
                <div class="card-action">
                    <a class="right" href="#">Add Schedule</a>
                </div>
            </div>
        </div>
        <div class="col s12 l3 m4 ">
            <div class="card small hoverable">
                <div class="card-image">
                    <img src="/public/patients/03.png">
                    <span class="card-title name">San Goku</span>
                </div>
                <div class="card-content center">
                    <p>5'7 ft | 82 Kgs</p>
                    <hr/>
                    <p>No pending schedule</p>
                </div>
                <div class="card-action">
                    <a class="right" href="#">Add Schedule</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var options = {
        valueNames: ['name']
    };

    var userList = new List('patients', options);
</script>
