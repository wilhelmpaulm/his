<div class="card white black-text">
    <div class="card-content">
        <span class="card-title black-text">Physical Examination</span>
        <div class="row">
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s4">
                        <input type="text" value="" class="validate">
                        <label for="email">Height (CM)</label>
                    </div>
                    <div class="input-field col s4">
                        <input type="text" value="" class="validate">
                        <label for="email">Weight (KG)</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <input type="text" value="" class="validate">
                        <label for="email">Blood Pressure</label>
                    </div>
                    <div class="input-field col s4">
                        <input type="text" value="" class="validate">
                        <label for="email">Pulse Rate/Minute</label>
                    </div>

                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <input type="text" value="" class="validate">
                        <label for="email">Temperature(Celsius)</label>
                    </div>
                    <div class="input-field col s4">
                        <input type="text" value="" class="validate">
                        <label for="email">Respiratory Rate / Minute</label>
                    </div>
                    <div class="input-field col s4">
                        <input type="text" value="" class="validate">
                        <label for="email">Visual Acuity</label>
                    </div>

                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <input type="text" value="" class="validate">
                        <label for="email">Hearing</label>
                    </div>

                </div>

            </form>
        </div>
    </div>
</div>
