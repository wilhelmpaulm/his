<?php include linkPage("template/header"); ?>
<body>
    <?php include linkPage("nav/header"); ?>
    <div class="container">
        <div class="section" >
            <?php include linkPage("main/patients/parts/profile"); ?>
            <div class="row">
                <div class="col s12 m12">
                    <?php include linkPage("main/patients/parts/form_schedule"); ?>
                </div>
            </div>
        </div>
    </div>
    <?php // include linkPage("nav/footer"); ?>
</body>
<?php include linkPage("template/footer"); ?>