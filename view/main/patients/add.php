<?php include linkPage("template/header"); ?>
<body>
    <?php include linkPage("nav/header"); ?>
    <div class="container">
        <div class="section">
            <form action="<?= linkTo("patients") ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col s12">
                        <div class="card-panel white black-text">
                            <span class="card-title black-text">Add Patient</span>
                            <div class="row">
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="first_name" name="first_name" required="" type="text" class="validate">
                                            <label for="first_name">first name</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="middle_name" name="middle_name" type="text" class="validate">
                                            <label for="middle_name">middle name</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="last_name" name="last_name" required="" type="text" class="validate">
                                            <label for="last_name">last name</label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="birthday" required="" name="birthday" type="date" class="datepicker">
                                            <label for="birthday">birthday</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <select name="sex" class="validate">
                                                <?php foreach (getSexList() as $gen): ?>
                                                    <option value="<?= $gen ?>"> <?= $gen ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <label for="password">sex</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <select name="blood_type" class="validate">
                                                <?php foreach (getOptionList("blood_type") as $gen): ?>
                                                    <option value="<?= $gen ?>"> <?= $gen ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <label for="blood_type">blood type</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                       
                                        <div class="input-field col l4">
                                            <select name="civil_status" class="validate">
                                                <?php foreach (getOptionList("civil_status") as $gen): ?>
                                                    <option value="<?= $gen ?>"> <?= $gen ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <label for="password">civil status</label>
                                        </div>
                                        <div class="file-field input-field col l8">
                                            <div class="btn">
                                                <span>patient image</span>
                                                <input name="image" required="" accept='image/*' type="file">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="email" type="email" name="email"  required="" class="validate">
                                            <label for="email">Email</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="mobile" type="text" name="mobile"  required="" class="validate">
                                            <label for="mobile">mobile</label>
                                        </div>


                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="mother_first_name" name="mother_first_name" required="" type="text" class="validate">
                                            <label for="mother_first_name">mother first name</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="mother_middle_name" name="mother_middle_name" type="text" class="validate">
                                            <label for="mother_middle_name">mother middle name</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="mother_last_name" name="mother_last_name" required="" type="text" class="validate">
                                            <label for="mother_last_name">mother last name</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="father_first_name" name="father_first_name" required="" type="text" class="validate">
                                            <label for="father_first_name">father first name</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="father_middle_name" name="father_middle_name" type="text" class="validate">
                                            <label for="father_middle_name">father middle name</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="father_last_name" name="father_last_name" required="" type="text" class="validate">
                                            <label for="father_last_name">father last name</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="file-field input-field col l8">
                                            <textarea id="address" name="address" class="materialize-textarea"></textarea>
                                            <label for="address">home address</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="occupation" type="text" name="occupation"  required="" class="validate">
                                            <label for="occupation">occupation</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="company_name" type="text" name="company_name"  required="" class="validate">
                                            <label for="company_name">company name</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="nationality" type="text" name="nationality"  required="" class="validate">
                                            <label for="nationality">nationality</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="religion" type="text" name="religion"  required="" class="validate">
                                            <label for="religion">religion</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="file-field input-field col l8">
                                            <textarea id="company_address" name="company_address" class="materialize-textarea"></textarea>
                                            <label for="company_address">company address</label>
                                        </div>

                                    </div>
                                </div>
                              
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col  offset-l8 l4">
                                            <button class="btn waves-effect waves-light block" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <?php // include linkPage("nav/footer"); ?>
</body>
<?php include linkPage("template/footer"); ?>