<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <head>
        <title>heartbase a holistic health information system</title>
        <link rel="icon" href="/public/icon.png" sizes="32x32">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!--<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->
        <link href="/public/css/icon.css" rel="stylesheet"/>
        <link href="/public/components/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="/public/components/Materialize/dist/css/materialize.min.css" rel="stylesheet"/>
        <link href="/public/components/chartist/dist/chartist.min.css" rel="stylesheet"/>

        <script src="/public/components/jquery/dist/jquery.min.js"></script>
        <script src="/public/components/Materialize/dist/js/materialize.js" ></script>
        <script src="/public/components/list.js/dist/list.min.js"></script>
        <script src="/public/components/chartist/dist/chartist.js"></script>
        <!--<script src="/public/components/chartist/dist/chartist.min.js.map"></script>-->
        <script src="/public/js/init.js"></script>
        <style>
            body, ul.side-nav {
                /*background-image: url("/public/bg/stardust.png");*/
                background-image: url("/public/bg/plaid.png");
            }
            .side-nav li {
                background: white;
            }
            label, div, span, input, select, i, a, p, textarea, form,
            .switch label .lever, 
            .switch label .lever:after{
                border-radius: 0 !important;
            }
            label {
                font-size: x-small;
            }
            .block {
                width: 100% !important; 
            }
            .card-title {
                color: #fff;
                font-size: 24px;
                font-weight: 300; 
            }
            .dropdown-content .select-dropdown {
                max-height: 300px;
            }
            .dropdown-content li>a, .dropdown-content li>span {
                font-size: 1rem;
            }
            hr {
                border-color: #EF5350 !important;
                color: #EF5350 !important;
                background-color: #EF5350 !important;
            }
        </style>
        <script>
            $("document").ready(function () {
                $('.datepicker.year').pickadate({
                    closeOnSelect: false,
                    selectMonths: false, // Creates a dropdown to control month
                    selectYears: 15 // Creates a dropdown of 15 years to control year
                });
                $('.datepicker').pickadate({
                    closeOnSelect: true,
                    format: 'yyyy-mm-dd',
                    formatSubmit: 'yyyy-mm-dd',
                    selectMonths: true, // Creates a dropdown to control month
                    selectYears: 15 // Creates a dropdown of 15 years to control year
                });
//                $("div.input-field").addClass("hoverable");
                $("nav").addClass("hoverable");
                $(".card").addClass("hoverable");
                $(".card-panel").addClass("hoverable");

                $.fn.isValid = function () {
                    return this[0].checkValidity();
                }
                //usage $('.isvalid').isValid();
                 $(".tab.off a").click(function () {
                    window.location.href = $(this).attr("href");
                    window.location.replace($(this).attr("href"));
                });

                $('.modal-trigger').leanModal();
            });
        </script>
    </head>

