<?php include linkPage("template/header"); ?>
<body>
    <?php include linkPage("nav/header"); ?>
    <div class="container">
        <div class="section">
            <form action="<?= linkTo("users") ?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col s12">
                        <div class="card-panel white black-text">
                            <span class="card-title black-text">Register</span>
                            <div class="row">
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="first_name" name="first_name" required="" type="text" class="validate">
                                            <label for="first_name">first name</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="middle_name" name="middle_name" type="text" class="validate">
                                            <label for="middle_name">middle name</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="last_name" name="last_name" required="" type="text" class="validate">
                                            <label for="last_name">last name</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="birthday" required="" name="birthday" type="date" class="datepicker">
                                            <label for="birthday">birthday</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <select name="gender" class="validate">
                                                <?php foreach (getSexList() as $gen): ?>
                                                    <option value="<?= $gen ?>"> <?= $gen ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <label for="password">gender</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">

                                        <div class="input-field col l4">
                                            <input id="mobile" type="text" name="mobile"  required="" class="validate">
                                            <label for="mobile">mobile</label>
                                        </div>
                                        <div class="file-field input-field col l8">
                                            <div class="btn">
                                                <span>user image</span>
                                                <input name="image" required="" accept='image/*' type="file">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="file-field input-field col l8">
                                            <textarea id="address" name="address" class="materialize-textarea"></textarea>
                                            <label for="address">address</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <select name="type" class="validate">
                                                <?php foreach (getUserTypesList() as $gen): ?>
                                                    <option value="<?= $gen ?>"> <?= $gen ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <label for="type">type</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <select name="specialization" class="validate">
                                                <?php foreach (getSpecializationList() as $gen): ?>
                                                    <option value="<?= $gen ?>"> <?= $gen ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <label for="specialization">specialization</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col l4">
                                            <input id="email" type="email" name="email"  required="" class="validate">
                                            <label for="email">Email</label>
                                        </div>
                                        <div class="input-field col l4">
                                            <input id="password" type="password" name="password" required="" class="validate">
                                            <label for="password">Password</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field col  offset-l8 l4">
                                            <button class="btn waves-effect waves-light block" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <?php // include linkPage("nav/footer"); ?>
</body>
<script>
    $("document").ready(function () {
        $('[name="type"]').ready(function () {
            if ($(this).val() != "doctor") {
                $('[name="specialization"]').attr("disabled", "").parent().parent().hide();
            } else {
                $('[name="specialization"]').removeAttr("disabled").parent().parent().show();
            }
        });
        $('[name="type"]').change(function () {
            if ($(this).val() != "doctor") {
                $('[name="specialization"]').attr("disabled", "").parent().parent().hide();
            } else {
                $('[name="specialization"]').removeAttr("disabled").parent().parent().show();
            }
        });
    });
</script>
<?php include linkPage("template/footer"); ?>