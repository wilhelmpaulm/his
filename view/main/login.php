<?php include linkPage("template/header"); ?>
<body>
    <?php include linkPage("nav/header"); ?>
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12  offset-m3 m6 offset-l4 l4">
                    <div class="card white black-text">
                        <div class="card-content">
                            <span class="card-title black-text">Login</span>
                        </div>
                        <div class="card-content">
                            <div class="row">
                                <form action="<?= linkTo("login")?>" method="POST" class="col s12">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="email" name="email" type="email" class="validate">
                                            <label for="email">email</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="password" name="password" type="password" class="validate">
                                            <label for="password">password</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button class="btn waves-effect waves-light block" type="submit" name="action">Submit
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php // include linkPage("nav/footer"); ?>
</body>
<?php include linkPage("template/footer"); ?>