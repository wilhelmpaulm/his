<div id="modal_schedules_add" class="modal modal-fixed-footer" style="">
    <div class="modal-content">
        <h4>add schedule</h4>
        <p>these schedules are the only ones allowed to use your keys</p>
        <div id="modal_schedules_add_form"></div>
    </div>
    <div class="modal-footer stars">
        <a href="#!" class=" modal-action modal-close waves-effect waves-light btn-flat light-blue-text">close</a>
    </div>
</div>