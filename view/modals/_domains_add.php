<div id="modal_domains_add" class="modal modal-fixed-footer" style="">
    <div class="modal-content">
        <h4>add api domain</h4>
        <p>these domains are the only ones allowed to use your keys</p>
        <?php include linkPage("forms/domains_add")?>
    </div>
    <div class="modal-footer stars">
        <a href="#!" class=" modal-action modal-close waves-effect waves-light btn-flat light-blue-text">close</a>
    </div>
</div>