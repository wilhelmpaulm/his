<div id="modal_domains_edit" class="modal modal-fixed-footer" style="">
    <div class="modal-content ">
        <h4>edit api domain</h4>
        <p>these domains are the only ones allowed to use your keys</p>
        <div id="modal_domains_edit_form"></div>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-light btn-flat">close</a>
    </div>
</div>