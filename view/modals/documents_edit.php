<div id="modal_documents_edit" class="modal modal-fixed-footer" style="">
    <div class="modal-content ">
        <h4>edit document</h4>
        <p></p>
        <div id="modal_documents_edit_form"></div>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-light btn-flat">close</a>
    </div>
</div>