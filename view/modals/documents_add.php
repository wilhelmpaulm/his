<div id="modal_documents_add" class="modal modal-fixed-footer" style="">
    <div class="modal-content">
        <h4>add document</h4>
        <p></p>
        <?php include linkPage("forms/documents_add")?>
    </div>
    <div class="modal-footer stars">
        <a href="#!" class=" modal-action modal-close waves-effect waves-light btn-flat light-blue-text">close</a>
    </div>
</div>