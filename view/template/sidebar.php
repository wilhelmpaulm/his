<div id="sidebar-wrapper" class="">
    <ul class="sidebar-nav ">
        <li class="sidebar-head ">
            <a href="#"  class="dsub" style="">
                SEATS API
            </a>
        </li>
        <li>
            <a class="dsub" href="#_home"><i class="fa fa-home"></i> Home</a>
        </li>
        <li>
            <a class="dsub" href="#_users"><i class="fa fa-users"></i> Users</a>
        </li>
        <li>
            <a class="dsub" href="#_login"><i class="fa fa-lock"></i> Login</a>
        </li>
        <li>
            <a class="dsub" href="#_favorites"><i class="fa fa-heart"></i> Favorites</a>
        </li>
        <li>
            <a class="dsub" href="#_ratings"><i class="fa fa-star"></i> Ratings</a>
        </li>
        <li>
            <a class="dsub" href="#_restaurants"><i class="fa fa-coffee"></i> Restaurants</a>
        </li>
        <li>
            <a class="dsub" href="#_reservations"><i class="fa fa-file-text"></i> Reservations</a>
        </li>
        <li>
            <a class="dsub" href="#_lists"><i class="fa fa-list-alt"></i> Lists</a>
        </li>
        <li>
            <a class="dsub" href="#_tags"><i class="fa fa-tags"></i> Tags</a>
        </li>
        <li>
            <a class="dsub" href="#_filters"><i class="fa fa-filter"></i> Filters</a>
        </li>
        <li>
            <a class="dsub" href="#_promotions"><i class="fa fa-ticket"></i> Promotions</a>
        </li>
        <li>
            <a class="dsub" href="#_gcm"><i class="fa fa-signal"></i> GCM</a>
        </li>
    </ul>
</div>

<div style="height: 3em; line-height: 3em;" class="bg-sunflower clearfix hidden-print"> 
    <div style="padding-left: 1em" class="c-white pull-left">
        <span id="menu-toggle" onclick="" style="cursor: pointer">
            <i  class="fa fa-bars"></i> TOGGLE SIDE BAR
        </span>
    </div>
    <div style="padding-left: 1em" class="c-white pull-left">
        <span id="menu-toggle-print" onclick="" style="cursor: pointer">
            <i  class="fa fa-print"></i> PRINT
        </span>
    </div>
    <div style="padding-right: 1em" class="c-white pull-right">
        <div id="clockbox"></div>
    </div>
</div>
<!--<div   style="height: 4em; background: rgba(249,249,249,1); line-height: 4em; text-indent: 1em; " class="">
    <a href="#" class="c-sunflower fs20 back"><i class="fa fa-chevron-left fa-fw"></i> </a> 
    <span href="#" class="c-gray fs20" id="header-address"> BACK</span>
</div>-->
<br/>

<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $("#menu-toggle-print").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        window.print();
    });
    $(".dd").hide();
    $(".ddtoggle").click(function (e) {
        e.preventDefault();
        $(this).siblings(".dd").toggle("fast");
    });
    $("#header-address").text($("a[href='<?= "http://" . $_SERVER["HTTP_HOST"] . explode("?", $_SERVER["REQUEST_URI"])[0] ?>']").text().toUpperCase());
    $("a[href='<?= "http://" . $_SERVER["HTTP_HOST"] . explode("?", $_SERVER["REQUEST_URI"])[0] ?>']").addClass("active").parent().parent(".dd").toggle("fast");
    $("a[href='<?= "http://" . $_SERVER["HTTP_HOST"] . explode("?", $_SERVER["REQUEST_URI"])[0] ?>']").addClass("active").parent().parent().siblings(".ddtoggle").find("a").addClass("active");
</script>