
<?php
$toast_message = "";
if (session("action") != null):
    switch (session("action")) :
        case "login_success":
            $toast_message = "welcome back";
            break;
        case "login_failed":
            $toast_message = "login failed, the invalid username or password. please try again";
            break;
        case "register_exists":
            $toast_message = "the email you are trying to use is already registered";
            break;
        case "schedule_success":
            $toast_message = "schedule was successfully added";
            break;
        case "schedule_delete":
            $toast_message = "schedule was successfully deleted";
            break;
        case "schedule_done":
            $toast_message = "schedule was marked done";
            break;
        default:
            break;
    endswitch;
endif;
?>
<?php if (session("action") != null): ?>
    <script>
        Materialize.toast('<?= $toast_message ?>', 5000);
    </script>
    <?php session("action", "unset"); ?>
<?php endif; ?>
</html>
