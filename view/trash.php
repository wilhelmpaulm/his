
<div class="col m12 l4">
    <div class="card medium">
        <div class="card-image waves-effect waves-block waves-light">
            <img class="activator" src="/public/bg/boxed-bg.jpg">
        </div>
        <div class="card-content">
            <span class="card-title activator grey-text text-darken-4">What's eSalin?<i class="material-icons right">more_vert</i></span>
            <p><a href="#">find out more</a></p>
        </div>
        <div class="card-reveal">
            <span class="card-title grey-text text-darken-4">What's eSalin<i class="material-icons right">close</i></span>
            <p>Here is some more information about this product that is only revealed once clicked on.</p>
        </div>
    </div>
</div>