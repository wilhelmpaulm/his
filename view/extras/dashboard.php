<?php include linkPage("template/header"); ?>
<div id="wrapper" class="hidden-print">

    <!-- Sidebar -->
    <?php include linkPage("template/sidebar"); ?>
    <!-- /#sidebar-wrapper -->
    <!--<a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>-->


    <!-- Page Content -->
    <div id="page-content-wrapper">
        <br/>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="">General Information</span>

                    </div> 
                    <div class="panel-body" style="overflow: auto">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="bg-red" style="margin: auto">
                                    <?php if ($user['picture']): ?>
                                        <div class="bg-black" style="width: 100%">
                                            <img class="img img-thumbnail" src="<?= linkPublic("pictures/{$user['picture']}") ?>" width="100%" height="50%" style=""/>
                                        </div>
                                    <?php else: ?>
                                        <div class="text-center well" style="width: 100%; height: 100%">
                                            NO PICTURE
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-condensed table-bordered" >
                                    <thead>
                                        <tr class="">
                                            <th>Details</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php if ($user["resume"]): ?>
                                            <tr>
                                                <td>RESUME</td>
                                                <td><a class="btn btn-primary btn-xs" href="<?= linkPublic("resumes/{$user["resume"]}") ?>" target="_blank"> <i class="fa fa-download"></i> Download Resume</a></td>
                                            </tr>
                                        <?php endif; ?>
                                        <tr>
                                            <td>CODE</td>
                                            <?php if ($user["code"]): ?>
                                                <td>CODE SET</td>
                                            <?php else: ?>
                                                <td><a  target="_blank" href="http://developer.globelabs.com.ph/dialog/oauth?app_id=<?= getInit("app_id") ?>"  class="c-white btn btn-xs btn-primary
                                                        "><i class="fa fa-question-circle c-white"></i> Allow Mobile Alerts</a></td>
                                                <?php endif; ?>
                                        </tr>
                                        <?php foreach ($user as $u => $v): ?>
                                            <?php if ($v && in_array($u, ["last_name", "first_name", "middle_name", "birthday", "gender", "type", "email", "contact_number"])): ?>
                                                <tr>
                                                    <td><?= strtoupper($u) ?></td>
                                                    <td><?= ucwords($v) ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>


                            </div><!-- /.box -->
                        </div><!-- /.box -->
                    </div><!-- /.box -->
                </div><!-- /.box -->
            </div>

        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="">Notifications</span>
                    </div> 
                    <div class="panel-body" style="overflow: auto">
                        <table class="table dtable table-bordered  table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th width='15%'>Date Created</th>
                                    <th width='5%'>Type</th>
                                    <th width='10%'>From</th>
                                    <th width='10%'>To</th>
                                    <th>Message</th>
                                    <th width='5%'>Status</th>
                                    <th width='1%'></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($notifications as $n): ?>
                                    <?php
                                    $from = [];
                                    $to = [];
                                    foreach ($users as $u):
                                        if ($n["id_from"] == $u["id"]):
                                            $from = $u;
                                        endif;
                                        if ($n["id_to"] == $u["id"]):
                                            $to = $u;
                                        endif;
                                    endforeach;
                                    ?>
                                    <tr>
                                        <td class="text-center"> <span class="label label-default"><?= $n["date_created"] ?></span></td>
                                        <td class="text-center">
                                            <?php if ($n["type"] == "log"): ?>
                                                <span class="label label-primary">LOG</span>
                                            <?php else: ?>
                                                <span class="label label-warning">ALERT</span>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <span class="">
                                                <?= $from["last_name"] . ", " . $from["first_name"] ?>
                                            </span>
                                        </td>
                                        <td> 
                                            <span class="">
                                                <?= $to["last_name"] . ", " . $to["first_name"] ?>
                                            </span>
                                        </td>
                                        <td>
                                            <p>
                                                <?= $n["message"] ?> 
                                            </p>
                                        </td>
                                        <td class="text-center">
                                            <?php if ($n["status"] == "read"): ?>
                                                <span class="label label-success">READ</span>
                                            <?php else: ?>
                                                <span class="label label-danger">UNREAD</span>
                                            <?php endif; ?>
                                        </td>
                                        <td  >
                                            <div class="btn-group btn-block " style="width: 12em">
                                                <?php if ($n["status"] != "read" && $n["id_to"] == getUser("id")): ?>
                                                    <a href="<?= linkTo("notifications/read/{$n['id']}") ?>" class="btn btn-primary btn-block btn-xs"><i class="fa fa-check-circle"></i> Mark as Read</a>
                                                <?php endif; ?>
                                                <?php if ($n["status"] == "unread" && $n["id_from"] == getUser("id")): ?>
                                                    <a href="<?= linkTo("notifications/delete/{$n['id']}") ?>" class="btn btn-danger  btn-block btn-xs"><i class="fa fa-minus-circle"></i> Delete</a>
                                                <?php endif; ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#page-content-wrapper -->

</div>
<?php include linkPage("template/footer"); ?>