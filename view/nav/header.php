<nav class="red lighten-1 hoverable" role="navigation">
    <div class="nav-wrapper container">
        <a id="" href="<?= linkTo("") ?>" class="brand-logo">heart <span class="fa fa-heartbeat"></span> base</a>
        <ul class="right hide-on-med-and-down">
            <?php if (user() != null): ?> 
                <?php if (in_array(user("type"), ["doctor"])): ?> 
                    <li><a href="<?= linkTo("users/" . user("id") . "/availability") ?>">availability</a></li>
                <?php endif; ?> 
                <li><a href="<?= linkTo("patients") ?>">view patients</a></li>
                <?php if (in_array(user("type"), ["nurse", "doctor"])): ?> 
                    <li><a href="<?= linkTo("schedules") ?>">schedules</a></li>
                    <li><a href="<?= linkTo("patients/add") ?>">add new patient</a></li>
                <?php endif; ?> 
                <?php if (in_array(user("type"), ["admin"])): ?> 
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown2">employees <span class="fa fa-caret-down"></span></a></li>
                    <ul id="dropdown2" class="dropdown-content">
                        <li><a href="<?= linkTo("users") ?>">view employees</a></li>
                        <li><a href="<?= linkTo("register") ?>">add new employee</a></li>
                    </ul>
                <?php endif; ?> 
                <?php if (in_array(user("type"), ["medtech"])): ?> 
                    <li><a href="<?= linkTo("documents") ?>">documents</a></li>
                <?php endif; ?> 
                <li><a href="<?= linkTo("users/" . user("id")) ?>">profile</a></li>
                <li><a href="<?= linkTo("logout") ?>">logout</a></li> 
            <?php else: ?> 
                <li><a href="<?= linkTo("register") ?>">register</a></li> 
                <li><a href="<?= linkTo("login") ?>">login</a></li> 
            <?php endif; ?> 
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <?php if (user() != null): ?> 
                <?php if (in_array(user("type"), ["doctor"])): ?> 
                    <li><a href="<?= linkTo("users/" . user("id") . "/availability") ?>">availability</a></li>
                <?php endif; ?> 
                <?php if (in_array(user("type"), ["nurse", "doctor"])): ?> 
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown11">patients <span class="fa fa-caret-down"></span></a></li>
                    <ul id="dropdown11" class="dropdown-content">
                        <li><a href="<?= linkTo("patients") ?>">view patients</a></li>
                        <li><a href="<?= linkTo("patients/add") ?>">add new patient</a></li>
                    </ul>
                    <li><a href="<?= linkTo("schedules") ?>">schedules</a></li>
                <?php endif; ?> 
                <?php if (in_array(user("type"), ["admin"])): ?> 
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown22">employees <span class="fa fa-caret-down"></span></a></li>
                    <ul id="dropdown22" class="dropdown-content">
                        <li><a href="<?= linkTo("users") ?>">view employees</a></li>
                        <li><a href="<?= linkTo("register") ?>">add new employee</a></li>
                    </ul>
                <?php endif; ?> 
                <?php if (in_array(user("type"), ["medtech"])): ?> 
                    <li><a href="<?= linkTo("documents") ?>">documents</a></li>
                <?php endif; ?> 
                <li><a href="<?= linkTo("logout") ?>">logout</a></li> 
            <?php else: ?> 
                <li><a href="<?= linkTo("register") ?>">register</a></li> 
                <li><a href="<?= linkTo("login") ?>">login</a></li> 
            <?php endif; ?> 
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>