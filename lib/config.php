<?php

global $database;
date_default_timezone_set('Asia/Manila');
session_save_path('temp/sessions');

$database = new medoo([
    'database_type' => 'mysql',
    'database_name' => init("db_name"),
    'server' => 'localhost',
    'username' => init("db_username"),
    'password' => init("db_password"),
    'charset' => 'utf8',
        ]);


function init($id = null) {
    $status = "offline";
    $init = [];
    if ($status == "offline") {
        $init = [
            "status" => "offline",
            "errors" => "on",
            "db_name" => "his",
            "db_username" => "root",
            "db_password" => "",
            "localhost" => "localhost:8000",
            "queue_url" => "localhost:7778",
            "dir" => $_SERVER['DOCUMENT_ROOT'],
            "api_url" => "http://stagingapi.seats.com.ph/",
            "partner_code" => "seats",
            "partner_auth_code" => "C01CF784-794D-4CAA-A4C3-37AE69E631E7",
            "widget_id" => "54728d3355bda4ff32479560",
            "gcm_api_key" => "AIzaSyBJj0h4iUv9Kc0haPR92BLXqMteChGnd54",
            "mail_email" => "wilhelmpaulm@gmail.com",
            "mail_password" => "pawie2062",
            "mail_name" => "wilhelmpaulm",
            "mail_cc" => "",

        ];
    } else if ($status == "online") {
        $init = [
            "status" => "online",
            "errors" => "off",
            "db_name" => "his",
            "db_username" => "root",
            "db_password" => "p@ssword",
            "status" => "online",
            "localhost" => "52.74.54.234:7777",
            "queue_url" => "localhost:7778",
            "dir" => $_SERVER['DOCUMENT_ROOT'],
            //"api_url" => "http://webapi.seats.com.ph/",
            "api_url" => "http://stagingapi.seats.com.ph/",
            "partner_code" => "seats",
            "partner_auth_code" => "C01CF784-794D-4CAA-A4C3-37AE69E631E7",
            "widget_id" => "54728d3355bda4ff32479560",
            "gcm_api_key" => "AIzaSyBJj0h4iUv9Kc0haPR92BLXqMteChGnd54",
            "mail_email" => "wilhelmpaulm@gmail.com",
            "mail_password" => "pawie2062",
            "mail_name" => "wilhelmpaulm",
            "mail_cc" => "",

        ];
    } else {
        $init = [
            "status" => "offline",
            "localhost" => "localhost",
            "dir" => $_SERVER['DOCUMENT_ROOT'],
            "disquis" => "wilhelmpaulm-test",
            "errors" => "on",
        ];
    }

    if ($id == null) {
        return $init;
    } else {
        return $init[$id];
    }
}
