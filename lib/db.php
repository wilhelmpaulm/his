<?php

function selectTable($table, $data = null) {
    global $database;

    if ($data == null) {
        $data = $database->select($table, "*");
    } else {
        $data = $database->select($table, "*", $data);
    }
    if (init("status") == "errors") {
        var_dump($database->error());
    }
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function deleteTable($table, $data) {
    global $database;

    if (is_array($data)) {
        $data = $database->delete($table, $data);
    } else {
        $data = $database->delete($table, ["id" => $data]);
    }
    if (init("status") == "errors") {
        var_dump($database->error());
    }
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function getTable($table, $data) {
    global $database;
    try {
        if (is_array($data)) {
            $data = $database->get($table, "*", $data);
        } else {
            $data = $database->get($table, "*", ["id" => $data]);
        }
    } catch (Exception $exc) {
//        echo $exc->getTraceAsString();
    }
    if (init("status") == "errors") {
        var_dump($database->error());
    }
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}

function insertTable($table, $data) {
    global $database;
    $data = $database->insert($table, $data);
    if (init("status") == "errors") {
    }
        var_dump($database->error());
//    die();
    if (!isset($data)) {
        return null;
    } else {
        return $data;
    }
}

function updateTable($table, $data, $id) {
    global $database;
//    $data["date_updated"] = currentdatetime();
    if (is_array($data) && !is_array($id)) {
        $data = $database->update($table, $data, ["id" => $id]);
    } else if (is_array($data) && is_array($id)) {
        $data = $database->update($table, $data, $id);
    } else {
        $data = "invalid parameters";
    }
    if (init("status") == "errors") {
    }
//    die();
    var_dump($database->error());
    var_dump($database);
    if (!isset($data) || $data == null) {
        return [];
    } else {
        return $data;
    }
}
