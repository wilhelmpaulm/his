<?php

$patients = selectTable("patients");
$users = selectTable("users");

function seekTable($table, $id = null) {
    global $$table;
    if ($$table && is_array($id)):
        foreach ($$table as $key => $val):
            $is = true;
            foreach ($id as $i => $d):
                if ($val[$i] != $d):
                    $is = false;
                endif;
            endforeach;
            if ($is):
                return $val;
            endif;
        endforeach;
    elseif ($$table && $id != null):
        foreach ($$table as $key => $val):
            if ($val["id"] == $id):
                return $val;
            endif;
        endforeach;
    elseif ($$table && $id == null):
        return $$table;
    else:
        return null;
    endif;
}

function getDayList() {
    $data = [
        "sunday",
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
    ];
    return $data;
}

function getSexList() {
    $data = [
        "male",
        "female",
    ];
    return $data;
}

function getUserTypesList() {
    $data = [
        "admin",
        "doctor",
        "medtech",
        "nurse",
    ];
    return $data;
}

function getSpecializationList() {
    $data = [
        "allergy and immunology",
        "cardiology adult general",
        "cardiology pediatric",
        "dental medicine",
        "pulmonary medicine",
        "radiology",
        "rehabilitation medicine",
        "rheumatology",
        "sleep medicine",
        "surgery general"
    ];

    return $data;
}

function getSpecializationOptionList() {
    $data = [
        [
            "specialization" => "allergy and immunology",
            "list" => [
                "allergic reaction",
                "swelling",
                "sneezing and a blocked, itchy or runny nose",
                "Itchiness",
                "Skin redness",
                "Dry, scaly, or crusted skin that might become thick and
                leathery from long-term scratching Formation of small, 
                fluid-filled blisters that might ooze when scratched",
                "Infection of the areas where the skin has been broken",
                "diarrhea",
                "Vomiting",
                "Anti Rabies",
                "Flu vaccine",
                "Hepatitis B vaccine",
                "diphtheria, tetanus (lockjaw), and pertussis (whooping cough)",
                "pneumococcal conjugate vaccine protects against a serious blood, lung, and brain infection",
                "Haemophilus influenzae type b, a serious brain, throat, and blood infection",
                "Polio: polio, a serious paralyzing disease",
                "RV: rotavirus infection, a serious diarrheal disease",
                "Influenza: a serious lung infection",
                "MMR: measles, mumps, and rubella",
                "HepA: hepatitis A, a serious liver disease",
                "Chickenpox: also called varicella"
            ],
        ],
        [
            "specialization" => "cardiology adult general",
            "list" => [
                "High Blood Pressure",
                "Aneurysm",
                "coronary circulation",
                "cardiac arrest",
                "myocardium",
                "pericardium",
                "hypertension",
                "Atherosclerosis",
                "Peripheral arterial disease",
                "Stroke",
                "Palpitation",
                "heart failure",
                "congenital heart disease"
            ],
        ],
        [
            "specialization" => "cardiology pediatric",
            "list" => [
                "High Blood Pressure",
                "Aneurysm",
                "coronary circulation",
                "cardiac arrest",
                "myocardium",
                "pericardium",
                "hypertension",
                "Atherosclerosis",
                "Peripheral arterial disease",
                "Stroke",
                "Palpitation",
                "heart failure",
                "congenital heart disease",
            ],
        ],
        [
            "specialization" => "dental medicine",
            "list" => [
                "Cold sores",
                "Canker sores",
                "Thrush",
                "Leukoplakia",
                "Dry mouth",
                "Gum or tooth problems",
                "Bad breath",
                "Lockjaw",
            ],
        ],
        [
            "specialization" => "pulmonary medicine",
            "list" => [
                "Asthma",
                "Lung Cancer",
                "Tuberculosis",
                "Occupational lung disease",
                "Pneumonia",
                "Emphysema"
            ],
        ],
        [
            "specialization" => "radiology",
            "list" => [
                "X-ray radiography",
                "ultrasound",
                "computed tomography (CT)",
                "positron emission tomography (PET)",
                "magnetic resonance imaging (MRI)",
            ],
        ],
        [
            "specialization" => "rehabilitation medicine",
            "list" => [
                "amputation",
                "spinal cord injury",
                "sports injury",
                "stroke",
                "musculoskeletal pain",
                "Chronic pain",
            ],
        ],
        [
            "specialization" => "rheumatology",
            "list" => [
                "rheumatism",
                "arthritis",
                "joint pains",
                "osteoporosis",
                "osteoarthritis",
                "tendinitis",
                "gout",
                "lupus",
                "back pain",
                "Spondyloarthropathies",
                "Tennis elbow",
                "Golfer's elbow",
                "Olecranon bursitis",
            ],
        ],
        [
            "specialization" => "sleep medicine",
            "list" => [
                "Sleep apnea",
                "Sleep deprivation",
                "narcolepsy",
                "hypersomnia",
                "Kleine-Levin syndrome",
                "dyssomnias",
                "parasomnias",
                "bruxism (tooth-grinding)",
                "sleepwalking",
                "bedwetting",
                "Polysomnography",
                "oximetry"
            ],
        ],
        [
            "specialization" => "surgery general",
            "list" => [
                "Trauma surgery / Surgical Critical Care",
                "Laparoscopic surgery",
                "Colorectal surgery",
                "Breast surgery",
                "Vascular surgery",
                "Endocrine surgery",
                "Transplant surgery",
                "Surgical oncology",
                "Cardiothoracic surgery",
                "Pediatric Surgery",
                "Robotic surgery",
                "Diverticulitis",
                "Cricothyroidotomy"
            ],
        ],
    ];

    return $data;
}

function getRangeList() {
    $data = [
        "ph" => [
            "min" => 4.6,
            "max" => 8.0,
        ],
        "rbc" => [
            "min" => 4.50,
            "max" => 5.20,
        ],
        "hemoglobin" => [
            "min" => 140,
            "max" => 170,
        ],
        "hematocrit" => [
            "min" => 0.42,
            "max" => 0.51,
        ],
        "mcv" => [
            "min" => 80,
            "max" => 96,
        ],
        "mch" => [
            "min" => 27.5,
            "max" => 33.2,
        ],
        "mchc" => [
            "min" => 334,
            "max" => 355,
        ],
        "rdw" => [
            "min" => 11.6,
            "max" => 14.6,
        ],
        "wbc" => [
            "min" => 5.00,
            "max" => 10.00,
        ],
        "platelet_count" => [
            "min" => 200,
            "max" => 400,
        ],
        "total_prostate_specific_antigen" => [
            "min" => 0.000,
            "max" => 4.000,
        ],
    ];
    return $data;
}

function getOptionList($name = null, $name2 = null) {
    $data = [
        "file_types" => [
            "x-ray", "prescription", "findings", "blood test", "stool test", "urine test", "cat scan",
            "observations", "admission", "others"
        ],
        "blood_type" => [
            "o+", "o-", "a+", "a-", "b+", "b-", "ab+", "ab-"
        ],
        "civil_status" => [
            "single", "married", "widowed"
        ],
        "patients_profile" => [
            "patient_admitted_from" => [
                "er", "home", "doctor's clinic", "other hospital", "others"
            ],
            "patient_admitted_from" => [
                "er", "home", "doctor's clinic", "other hospital", "others"
            ],
            "mode_of_admission" => [
                "ambulatory", "wheelchair", "stretcher", "cuddled"
            ],
            "baptized" => [
                "yes", "no"
            ],
            "guardian" => [
                "mother", "father", "others"
            ],
            "neonatal_history" => [
                "full term", "pre-term"
            ],
            "deliver" => [
                "nsvd", "cs"
            ],
            "condition_at_birth" => [
                "normal", "abnormal"
            ],
            "condition_at_birth" => [
                "present", "absent"
            ],
            "orientation_given_to" => [
                "patient", "spouse", "relative", "others"
            ],
        ],
        "hospital_orientation" => [
            "oriented_to" => [
                "bathrooms", "bed controls", "bed rails", "billing",
                "call bedside nurse/manager", "food/meals", "pastrol services", "proper waste disposal",
                "room/unit setup polict", "smoking policy", "use of call system", "use of equipment",
                "waiting hours and policy"
            ],
            "temperature" => [
                "auxillary", "oral", "rectal", "tympanic", "others"
            ],
            "oriented_to" => [
                "person", "time", "place", "tympanic", "event"
            ],
            "headache" => [
                "yes", "no"
            ],
            "loss_of_consciousness" => [
                "yes", "no"
            ],
            "seizure_episode" => [
                "yes", "no"
            ],
            "eyes_open" => [
                "1" => "no response",
                "2" => "to pain",
                "3" => "to verbal command",
                "4" => "spontaneous",
            ],
            "best_verbal_response" => [
                "1" => "no response",
                "2" => "comprehensible sound",
                "3" => "innapropriate words",
                "4" => "confused / inappropriate words",
                "5" => "oriented",
            ],
            "best_motor_response" => [
                "1" => "no response",
                "2" => "extension to pain",
                "3" => "flexion to pain",
                "4" => "withdrawal to pain",
                "5" => "localy to pain",
                "6" => "obeys command",
            ],
            "heart_sounds" => [
                "normal", "abnormal"
            ],
            "heart_thrytm" => [
                "regular", "irregular"
            ],
            "pulse" => [
                "present", "weak", "absent"
            ],
            "chest_pain_scale" => [
                "1" => "mild pain relieved by rest",
                "2" => "mild pain relieved by mtg",
                "3" => "moderate pain relieved by ms04/denerol",
                "4" => "severe pain relieved by ms04/denerol",
                "4" => "intense",
            ],
            "chest_movement" => [
                "symmetrical", "asymetrical", "others"
            ],
            "oxygenation" => [
                "room air", "nasal cannula", "nasal catherer", "face mask",
                "endotracheal tube", "trache mask", "bpap", "cpap", "others"
            ],
            "bowel_sounds" => [
                "present", "absent"
            ],
            "abdomen" => [
                "normal", "enlarged", "rigid"
            ],
            "abdominal_pain" => [
                "present", "absent", "rigid"
            ],
            "urinary_pattern" => [
                "normal", "abnormal"
            ],
            "genital_area" => [
                "normal", "rash", "discharge", "lesions", "others"
            ],
            "genitourinary_pain" => [
                "present", "absent"
            ],
            "contraption" => [
                "indselling catherer", "condom catherer", "capo", "others"
            ],
            "gait" => [
                "normal", "abnormal"
            ],
            "rom" => [
                "normal", "abnormal"
            ],
            "musculoskeletal_pain" => [
                "present", "absent"
            ],
            "skin_integrity" => [
                "intact", "wound", "rash", "bruise", "hematoma", "others"
            ],
            "color" => [
                "normal", "pale", "cysnotic", "jaundice", "others"
            ],
            "temperature" => [
                "warm", "cool", "others"
            ],
            "moisture" => [
                "dry", "cool", "others"
            ],
            "turgor" => [
                "good", "poor"
            ],
            "edema" => [
                "none", "pitting", "non-pitting"
            ],
            "others" => [
                "none", "clubbing"
            ],
            "iv_access" => [
                "none", "peripheral_line", "central_line", "others"
            ],
            "iv_access_status" => [
                "parent", "intact", "dislodged", "others"
            ],
            "pressence_of_pressure" => [
                "yes", "no"
            ],
            "areas" => [
                "sacrum", "iscnial tuberosity", "throchanter", "lumbar vertebrae",
                "thoracic vertebrae", "scapula", "elbow", "lateral knee",
                "medial knee", "medial malleous inner ankle", "medial malleous outer ankle",
                "heel", "occipal", "ear", "buttocks",
            ],
            "stage of ulcer" => [
                "none", "1", "2", "3"
            ],
            "exudates" => [
                "yes", "no"
            ],
            "exudates_amount" => [
                "none", "scant", "moderate", "copious"
            ],
            "exudates_odor" => [
                "none", "foul"
            ],
            "wound_pain" => [
                "present", "absent"
            ],
            "wound_pain_scale" => [
                "0" => "no hurt",
                "1" => "hurts a little",
                "2" => "hurts a little more",
                "3" => "hurts even more",
                "4" => "hurts whole more",
                "5" => "hurts worst",
            ],
            "living_companion" => [
                "none", "spouse", "family", "significant others", "others"
            ],
            "behavior" => [
                "cooperative", "uncooperative", "agitated", "combative", "calm", "depressed", "mentally challenged", "others"
            ],
            "activities_of_daily_living" => [
                "self-care", "partial assistance", "total care", "physically challenged", "others"
            ],
        ],
        "health_history" => [
            "past_medical_history" => [
                "hpn", "cad", "dm", "cva", "others"
            ],
            "immunizations" => [
                "hepatitis b", "tetanus", "others"
            ],
            "sensory_perception" => [
                "1" => "completely limited",
                "2" => "very limited",
                "3" => "slightly limited",
                "4" => "no impairment",
            ],
            "moisture" => [
                "1" => "constantly moist",
                "2" => "very moist",
                "3" => "occasionally moist",
                "4" => "rarely moist",
            ],
            "activity" => [
                "1" => "bedfast",
                "2" => "chairfast",
                "3" => "walks occasionally",
                "4" => "walks frequently",
            ],
            "mobility" => [
                "1" => "completely immobile",
                "2" => "very limited",
                "3" => "slightly limited",
                "4" => "no limitations",
            ],
            "nutrition" => [
                "1" => "very poor",
                "2" => "probably inadequate",
                "3" => "adequate",
                "4" => "excellent",
            ],
            "friction_and_shear" => [
                "1" => "problem",
                "2" => "potential problem",
                "3" => "no apparent problem",
            ],
            "age" => [
                "4" => "0 - 6 years",
                "2" => "7 - 18 years",
                "1" => "19 - 64 years",
                "2" => "65 - 79 years",
                "3" => "80 years and above",
            ],
            "fall_history" => [
                "5" => "fall within 3 months beyond admission",
                "11" => "fall during hospitalization",
                "0" => "no history at all",
            ],
            "mobility2" => [
                "4" => "visual or auditory impairment affecting mobility",
                "2" => "ambulates or transfers with steady gate an no assistance or assistive devices",
                "2" => "ambulates or transfers with assistance or assistive device",
                "0" => "ambulates without assistance",
            ],
            "elimination" => [
                "2" => "urgency/nocturin",
                "5" => "incontinence",
                "0" => "normal pattern",
            ],
            "mental_status_changes" => [
                "4" => "affecting awereness of one's physical limitation",
                "2" => "affecting awareness of environment",
            ],
            "medications" => [
                "5" => "two or more",
                "3" => "one present",
                "0" => "no medication",
            ],
            "patientcare_equipment" => [
                "2" => ">= 2(iv line, chest tube, indwelling catherer, others)",
                "1" => "one present",
                "0" => "no equipment/gadget attached",
            ],
        ]
    ];
    if ($name != null && $name2 != null) :
        return $data[$name][$name2];
    elseif ($name != null) :
        return $data[$name];
    endif;
    return $data;
}

function getTableFormList($name = null) {
    $data = [
        "form_admissions" => [
            "form_braden_risk_assessments",
            "form_clinical_data_admission_vital_signs_assessments",
            "form_clinical_data_cardiovascular_assessments",
            "form_clinical_data_gastrointestinal_assessments",
            "form_clinical_data_genitourinary_assessments",
            "form_clinical_data_integumentary_assessments",
            "form_clinical_data_musculoskeletal_assessments",
            "form_clinical_data_neurological_assessments",
            "form_clinical_data_psychosocial_assessments",
            "form_clinical_data_respiratory_assessments",
            "form_fall_risk_assessments",
            "form_health_histories",
            "form_health_history_daily_living_habits",
            "form_health_history_obgyn_histories",
            "form_health_history_past_medical_histories",
            "form_hospital_orientations",
            "form_patient_profiles",
            "form_patient_profile_pediatrics",
        ]
    ];
    if ($name != null) {
        return $data[$name];
    }
    return $data;
}
