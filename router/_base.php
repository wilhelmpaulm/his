<?php

$router->get('/list/', function() {
    getSpecializationList();
});

$router->post('/test/', function() {
    echo '<h3>TEST</h3>';
    echo '<hr/>';
    echo '<h3>POST</h3>';
    echo '<pre>';
    var_dump(getPost());
    echo '</pre>';
    echo '<hr/>';
    echo '<h3>GET</h3>';
    echo '<pre>';
    var_dump(getGet());
    echo '</pre>';
});

$router->get('/test/', function() {
    echo '<h3>TEST</h3>';
    echo '<hr/>';
    echo '<h3>POST</h3>';
    echo '<pre>';
    var_dump(getPost());
    echo '</pre>';
    echo '<hr/>';
    echo '<h3>GET</h3>';
    echo '<pre>';
    var_dump(getGet());
    echo '</pre>';
//    $username = null;
//    $password = null;
//    var_dump($_SERVER);
//    die();
//
//// mod_php
//    if (isset($_SERVER['PHP_AUTH_USER'])) {
//        $username = $_SERVER['PHP_AUTH_USER'];
//        $password = $_SERVER['PHP_AUTH_PW'];
//
//// most other servers
//    } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
//        if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']), 'basic') === 0)
//            list($username, $password) = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
//    }
//
//    if (is_null($username)) {
//
//        header('WWW-Authenticate: Basic realm="My Realm"');
//        header('HTTP/1.0 401 Unauthorized');
//        echo 'Text to send if user hits Cancel button';
//
//        die();
//    } else {
//        echo "<p>Hello {$username}.</p>";
//        echo "<p>You entered {$password} as your password.</p>";
//    }
});
