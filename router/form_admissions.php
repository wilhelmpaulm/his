<?php

### form_admissions
### -> this is a generic form_admission 
### 
### get all options with the URL base form_admission
$router->options("/form_admissions/", function() {
    include linkPage("main/form_admissions/options");
});

### get single form_admissions
### return view
$router->get("/form_admissions/", function() {
    $form_admissions = selectTable("form_admissions");
    include linkPage("main/form_admissions/index");
});

### add single form_admissions
### return view
$router->get("/form_admissions/add", function() {
    include linkPage("main/form_admissions/add");
});

### get single form_admission
### return view
$router->get("/form_admissions/:id", function($id) {
    $form_admission = getTable("form_admissions", $id);
    include linkPage("main/patients/form_admissions");
});

### single single form_admission
### redirect back
$router->post("/form_admissions/", function() {
    $form_admission = [
        "id_user" => user("id"),
        "id_patient" => getPost("id_patient")
    ];
    $id_form_admission = insertTable("form_admissions", $form_admission);
    $base = [
        "id_form_admission" => $id_form_admission,
        "id_user" => user("id"),
        "id_patient" => getPost("id_patient")
    ];
    foreach (getTableFormList("form_admissions") as $fal) :
        insertTable($fal, $base);
    endforeach;

    $form_admission = getTable("form_admissions", $id_form_admission);
    include sendTo("patients/".  getPost("id_patient")."/form_admissions/".$id_form_admission);
});

$router->delete("/form_admissions/:id", function($id) {
    $form_admission = getTable("form_admissions", $id);
    $base = ["id_form_admission" => $id];
    foreach (getTableFormList("form_admissions") as $fal) :
        deleteTable($fal, $base);
    endforeach;

    $id = deleteTable("form_admissions", $id);
    include sendTo("back");
});
