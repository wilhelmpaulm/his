<?php

### schedules
### -> this is a generic schedule 
### 
### get all options with the URL base schedule
$router->options("/schedules/", function() {
    include linkPage("main/schedules/options");
});

### get single schedules
### return view
$router->get("/schedules/", function() {
    $schedules = selectTable("schedules");
    include linkPage("main/schedules/index");
});

### upodate single key
### redirect back
$router->get("/schedules/add/form/:day", function($day) {
    $availabilities = selectTable("user_availabilities", ["id_user" => getGet("id_user")]);
    $availability = [];
    foreach ($availabilities as $key => $ava):
        if ($key == $day):
            $availability = $ava;
        endif;
    endforeach;
    $ret = "";
    foreach (getTimeList() as $key => $tim):
        if ($tim > $availability["time_start"] && $tim < $availability["time_end"]):
            $ret.="<option>$tim</option>";
        endif;
    endforeach;
    echo $ret;
//    include linkPage("forms/schedules_add");
});

$router->get("/schedules/add/form/", function() {
    $patient = getTable("patients", getGet("id_patient"));
    $user = getTable("users", getGet("id_user"));
    $availabilities = selectTable("user_availabilities", ["id_user" => getGet("id_user")]);
//    $schedule = getTable("schedules", getPost("id"));
    include linkPage("forms/schedules_add");
});


### upodate single key
### redirect back
$router->get("/schedules/edit/form/", function() {
    $schedule = getTable("schedules", getPost("id"));
    include linkPage("forms/schedules_edit");
});

### upodate single key
### redirect back
$router->get("/schedules/view/form/", function() {
    $schedules = selectTable("schedules", ["id_user" => getGet("id_user")]);
//    var_dump($schedules);
//    die();
    include linkPage("forms/schedules_view");
});


### add single schedules
### return view
$router->get("/schedules/add", function() {
    include linkPage("main/schedules/add");
});

### get single schedule
### return view
$router->get("/schedules/:id", function($id) {
    $schedule = getTable("schedules", $id);
    include linkPage("main/schedules/single");
});

### single single schedule
### redirect back
$router->post("/schedules/", function() {
    $schedule = [
        "id_user" => getPost("id_user"),
        "id_patient" => getPost("id_patient"),
        "day" => getPost("day"),
        "time_start" => getPost("time_start"),
        "note" => getPost("note"),
        "status" => "pending"
    ];
    $id = insertTable("schedules", $schedule);
    $schedule = getTable("scheduless", $id);
    session("action", "schedule_success");
    include sendTo("patients");
});

### upodate single schedule
### redirect back
$router->post("/schedules/:id", function($id) {
    $old = getTable("schedules", $id);
    $total_score = getPost("moisture") + getPost("activity") + getPost("mobility") + getPost("nutrition") + getPost("sensory_perception") + getPost("friction_and_shear");
    if ($total_score < 12):
        $risk_category = "high risk";
    elseif ($total_score > 12 || $total_score < 15):
        $risk_category = "moderate risk";
    else:
        $risk_category = "low risk";
    endif;

    $schedule = [
        "sensory_perception" => getPost("sensory_perception"),
        "moisture" => getPost("moisture"),
        "activity" => getPost("activity"),
        "mobility" => getPost("mobility"),
        "nutrition" => getPost("nutrition"),
        "friction_and_shear" => getPost("friction_and_shear"),
        "total_score" => $total_score,
        "risk_category" => $risk_category,
        "status" => "filled"
    ];
    $id = updateTable("schedules", $schedule, $id);
    $schedule = getTable("schedules", $id);
    include sendTo("back");
});

### delete single schedule
### redirect back
$router->delete("/schedules/:id", function($id) {
    $schedule = getTable("schedules", $id);
    $id = deleteTable("schedules", $id);
    include sendTo("back");
});

### delete single schedule
### redirect back
$router->get("/schedules/:id/delete", function($id) {
    $schedule = getTable("schedules", $id);
    $id = deleteTable("schedules", $id);
    session("action", "schedule_delete");
    include sendTo("back");
});

### delete single schedule
### redirect back
$router->get("/schedules/:id/done", function($id) {
    updateTable("schedules", ["status" => "done"], $id);
    $schedule = getTable("schedules", $id);
    session("action", "schedule_done");
    include sendTo("back");
});
