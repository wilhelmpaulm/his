<?php

### form_clinical_data_admission_vital_signs_assessment
### -> this is a generic domain 
### 

### get all options with the URL base domain
$router->options("/form_clinical_data_admission_vital_signs_assessment/", function() {
    include linkPage("main/form_clinical_data_admission_vital_signs_assessment/options");
});

### get single form_clinical_data_admission_vital_signs_assessment
### return view
$router->get("/form_clinical_data_admission_vital_signs_assessment/", function() {
    $form_clinical_data_admission_vital_signs_assessment = selectTable("form_clinical_data_admission_vital_signs_assessment");
    include linkPage("main/form_clinical_data_admission_vital_signs_assessment/index");
});

### add single form_clinical_data_admission_vital_signs_assessment
### return view
$router->get("/form_clinical_data_admission_vital_signs_assessment/add", function() {
    include linkPage("main/form_clinical_data_admission_vital_signs_assessment/add");
});

### get single domain
### return view
$router->get("/form_clinical_data_admission_vital_signs_assessment/:id", function($id) {
    $domain = getTable("form_clinical_data_admission_vital_signs_assessment", $id);
    include linkPage("main/form_clinical_data_admission_vital_signs_assessment/single");
});

### single single domain
### redirect back
$router->post("/form_clinical_data_admission_vital_signs_assessment/", function() {
    $domain = [
        "blood_pressure" => getPost("blood_pressure"),
        "patient_position" => getPost("patient_position"),
        "time" => getPost("time"),
        "cardiac_rate" => getPost("cardiac_rate"),
        "respiratory_rate" => getPost("respiratory_rate"),
        "oxygen_saturation" => getPost("oxygen_saturation"),
        "temperature" => getPost("temperature"),
        "height" => getPost("height"),
        "weight" => getPost("weight")
    ];
    $id = insertTable("form_clinical_data_admission_vital_signs_assessment", $domain);
    $domain = getTable("form_clinical_data_admission_vital_signs_assessments", $id);
    include sendTo("back");
});

### upodate single domain
### redirect back
$router->post("/form_clinical_data_admission_vital_signs_assessment/:id", function($id) {
    $domain = [
        "blood_pressure" => getPost("blood_pressure"),
        "patient_position" => getPost("patient_position"),
        "time" => getPost("time"),
        "cardiac_rate" => getPost("cardiac_rate"),
        "respiratory_rate" => getPost("respiratory_rate"),
        "oxygen_saturation" => getPost("oxygen_saturation"),
        "temperature" => getPost("temperature"),
        "height" => getPost("height"),
        "weight" => getPost("weight"),
        "status" =>"filled"
    ];
    $id = updateTable("form_clinical_data_admission_vital_signs_assessment", $domain, $id);
    $domain = getTable("form_clinical_data_admission_vital_signs_assessment", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_clinical_data_admission_vital_signs_assessment/edit/form/", function() {
    $domain = getTable("form_clinical_data_admission_vital_signs_assessment", getPost("id_domain"));
    include linkPage("forms/form_clinical_data_admission_vital_signs_assessment_edit");
});

### delete single domain
### redirect back
$router->delete("/form_clinical_data_admission_vital_signs_assessment/:id", function($id) {
    $domain = getTable("form_clinical_data_admission_vital_signs_assessment", $id);
    $id = deleteTable("form_clinical_data_admission_vital_signs_assessment", $id);
    include sendTo("back");
});
