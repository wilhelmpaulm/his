<?php

### form_clinical_data_gastrointestinal_assessments
### -> this is a generic form_clinical_data_gastrointestinal_assessment 
### 
### get all options with the URL base form_clinical_data_gastrointestinal_assessment
$router->options("/form_clinical_data_gastrointestinal_assessments/", function() {
    include linkPage("main/form_clinical_data_gastrointestinal_assessments/options");
});

### get single form_clinical_data_gastrointestinal_assessments
### return view
$router->get("/form_clinical_data_gastrointestinal_assessments/", function() {
    $form_clinical_data_gastrointestinal_assessments = selectTable("form_clinical_data_gastrointestinal_assessments");
    include linkPage("main/form_clinical_data_gastrointestinal_assessments/index");
});

### add single form_clinical_data_gastrointestinal_assessments
### return view
$router->get("/form_clinical_data_gastrointestinal_assessments/add", function() {
    include linkPage("main/form_clinical_data_gastrointestinal_assessments/add");
});

### get single form_clinical_data_gastrointestinal_assessment
### return view
$router->get("/form_clinical_data_gastrointestinal_assessments/:id", function($id) {
    $form_clinical_data_gastrointestinal_assessment = getTable("form_clinical_data_gastrointestinal_assessments", $id);
    include linkPage("main/form_clinical_data_gastrointestinal_assessments/single");
});

### single single form_clinical_data_gastrointestinal_assessment
### redirect back
$router->post("/form_clinical_data_gastrointestinal_assessments/", function() {
    $form_clinical_data_gastrointestinal_assessment = [
        "bowel_sounds" => getPost("bowel_sounds"),
        "abdomen" => getPost("abdomen"),
        "abdominal_pain" => getPost("abdominal_pain"),
        "abdominal_pain_location" => getPost("abdominal_pain_location"),
        "abdominal_pain_scale" => getPost("abdominal_pain_scale"),
        "dental_status" => getPost("dental_status"),
        "dental_status_with_dentures" => getPost("dental_status_with_dentures"),
        "tube_feedings" => getPost("tube_feedings"),
        "tube_feedings_type" => getPost("tube_feedings_type"),
        "tube_feedings_volume" => getPost("tube_feedings_volume"),
        "ostomy_type" => getPost("ostomy_type"),
        "bowel_pattern" => getPost("bowel_pattern"),
        "bowel_pattern_last_lbm" => getPost("bowel_pattern_last_lbm")
    ];
    $id = insertTable("form_clinical_data_gastrointestinal_assessments", $form_clinical_data_gastrointestinal_assessment);
    $form_clinical_data_gastrointestinal_assessment = getTable("form_clinical_data_gastrointestinal_assessmentss", $id);
    include sendTo("back");
});

### upodate single form_clinical_data_gastrointestinal_assessment
### redirect back
$router->post("/form_clinical_data_gastrointestinal_assessments/:id", function($id) {
    $form_clinical_data_gastrointestinal_assessment = [
        "bowel_sounds" => getPost("bowel_sounds"),
        "abdomen" => getPost("abdomen"),
        "abdominal_pain" => getPost("abdominal_pain"),
        "abdominal_pain_location" => getPost("abdominal_pain_location"),
        "abdominal_pain_scale" => getPost("abdominal_pain_scale"),
        "dental_status" => getPost("dental_status"),
        "dental_status_with_dentures" => getPost("dental_status_with_dentures"),
        "tube_feedings" => getPost("tube_feedings"),
        "tube_feedings_type" => getPost("tube_feedings_type"),
        "tube_feedings_volume" => getPost("tube_feedings_volume"),
        "ostomy_type" => getPost("ostomy_type"),
        "bowel_pattern" => getPost("bowel_pattern"),
        "bowel_pattern_last_lbm" => getPost("bowel_pattern_last_lbm"),
        "status" => "filled"
    ];
    $id = updateTable("form_clinical_data_gastrointestinal_assessments", $form_clinical_data_gastrointestinal_assessment, $id);
    $form_clinical_data_gastrointestinal_assessment = getTable("form_clinical_data_gastrointestinal_assessments", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_clinical_data_gastrointestinal_assessments/edit/form/", function() {
    $form_clinical_data_gastrointestinal_assessment = getTable("form_clinical_data_gastrointestinal_assessments", getPost("id_form_clinical_data_gastrointestinal_assessment"));
    include linkPage("forms/form_clinical_data_gastrointestinal_assessments_edit");
});

### delete single form_clinical_data_gastrointestinal_assessment
### redirect back
$router->delete("/form_clinical_data_gastrointestinal_assessments/:id", function($id) {
    $form_clinical_data_gastrointestinal_assessment = getTable("form_clinical_data_gastrointestinal_assessments", $id);
    $id = deleteTable("form_clinical_data_gastrointestinal_assessments", $id);
    include sendTo("back");
});
