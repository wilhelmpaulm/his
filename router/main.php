<?php

### main page
$router->get('/', function() {
    include linkPage("main/main");
});

### login page
$router->get('/login/', function() {
    include linkPage("main/login");
});

### login page
$router->get('/logout/', function() {
    sessionStop();
    sendTo("");
});

### login page
$router->post('/login/', function() {
    $user = getTable("users", ["AND" => ["email" => getPost("email"), "password" => getPost("password")]]);
    if ($user):
        session("user", $user);
        sendTo("users/".  user("id"));
    else:
        session("action", "login_failed");
        sendTo("back");
    endif;
});

### registration page
$router->get('/register/', function() {
    include linkPage("main/register");
});

### upload test
$router->get('/upload/', function() {
    include linkPage("main/upload");
});

### upload test
$router->post('/upload/', function() {
    upload("upload", "", "saaa");
});

### upload test
$router->get('/list/', function() {
    
});
