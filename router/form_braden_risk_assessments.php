<?php

### form_braden_risk_assessments
### -> this is a generic form_braden_risk_assessment 
### 
### get all options with the URL base form_braden_risk_assessment
$router->options("/form_braden_risk_assessments/", function() {
    include linkPage("main/form_braden_risk_assessments/options");
});

### get single form_braden_risk_assessments
### return view
$router->get("/form_braden_risk_assessments/", function() {
    $form_braden_risk_assessments = selectTable("form_braden_risk_assessments");
    include linkPage("main/form_braden_risk_assessments/index");
});

### add single form_braden_risk_assessments
### return view
$router->get("/form_braden_risk_assessments/add", function() {
    include linkPage("main/form_braden_risk_assessments/add");
});

### get single form_braden_risk_assessment
### return view
$router->get("/form_braden_risk_assessments/:id", function($id) {
    $form_braden_risk_assessment = getTable("form_braden_risk_assessments", $id);
    include linkPage("main/form_braden_risk_assessments/single");
});

### single single form_braden_risk_assessment
### redirect back
$router->post("/form_braden_risk_assessments/", function() {
    $form_braden_risk_assessment = [
        "id_user" => getUser("id"),
        "sensory_perception" => getPost("sensory_perception"),
        "moisture" => getPost("moisture"),
        "activity" => getPost("activity"),
        "mobility" => getPost("mobility"),
        "nutrition" => getPost("nutrition"),
        "friction_and_shear" => getPost("friction_and_shear"),
        "total_score" => getPost("total_score"),
        "risk_category" => getPost("risk_category"),
        "status" => "filled"
    ];
    $id = insertTable("form_braden_risk_assessments", $form_braden_risk_assessment);
    $form_braden_risk_assessment = getTable("form_braden_risk_assessmentss", $id);
    include sendTo("back");
});

### upodate single form_braden_risk_assessment
### redirect back
$router->post("/form_braden_risk_assessments/:id", function($id) {
    $old = getTable("form_braden_risk_assessments", $id);
    $total_score = getPost("moisture") + getPost("activity") + getPost("mobility") + getPost("nutrition") + getPost("sensory_perception") + getPost("friction_and_shear");
    if ($total_score < 12):
        $risk_category = "high risk";
    elseif ($total_score > 12 || $total_score < 15):
        $risk_category = "moderate risk";
    else:
        $risk_category = "low risk";
    endif;
    
    $form_braden_risk_assessment = [
        "sensory_perception" => getPost("sensory_perception"),
        "moisture" => getPost("moisture"),
        "activity" => getPost("activity"),
        "mobility" => getPost("mobility"),
        "nutrition" => getPost("nutrition"),
        "friction_and_shear" => getPost("friction_and_shear"),
        "total_score" => $total_score,
        "risk_category" => $risk_category,
        "status" => "filled"
    ];
    $id = updateTable("form_braden_risk_assessments", $form_braden_risk_assessment, $id);
    $form_braden_risk_assessment = getTable("form_braden_risk_assessments", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_braden_risk_assessments/edit/form/", function() {
    $form_braden_risk_assessment = getTable("form_braden_risk_assessments", getPost("id"));
    include linkPage("forms/form_braden_risk_assessments_edit");
});

### upodate single key
### redirect back
$router->post("/form_braden_risk_assessments/view/form/", function() {
    $form_braden_risk_assessment = getTable("form_braden_risk_assessments", getPost("id"));
    $form = $form_braden_risk_assessment;
    include linkPage("forms/form_braden_risk_assessments_view");
});

### delete single form_braden_risk_assessment
### redirect back
$router->delete("/form_braden_risk_assessments/:id", function($id) {
    $form_braden_risk_assessment = getTable("form_braden_risk_assessments", $id);
    $id = deleteTable("form_braden_risk_assessments", $id);
    include sendTo("back");
});
