<?php

### form_fall_risk_assessments
### -> this is a generic form_fall_risk_assessment 
### 
### get all options with the URL base form_fall_risk_assessment
$router->options("/form_fall_risk_assessments/", function() {
    include linkPage("main/form_fall_risk_assessments/options");
});

### get single form_fall_risk_assessments
### return view
$router->get("/form_fall_risk_assessments/", function() {
    $form_fall_risk_assessments = selectTable("form_fall_risk_assessments");
    include linkPage("main/form_fall_risk_assessments/index");
});

### add single form_fall_risk_assessments
### return view
$router->get("/form_fall_risk_assessments/add", function() {
    include linkPage("main/form_fall_risk_assessments/add");
});

### get single form_fall_risk_assessment
### return view
$router->get("/form_fall_risk_assessments/:id", function($id) {
    $form_fall_risk_assessment = getTable("form_fall_risk_assessments", $id);
    include linkPage("main/form_fall_risk_assessments/single");
});

### single single form_fall_risk_assessment
### redirect back
$router->post("/form_fall_risk_assessments/", function() {
    $form_fall_risk_assessment = [
        "age" => getPost("age"),
        "fall_history" => getPost("fall_history"),
        "mobility" => getPost("mobility"),
        "elimination" => getPost("elimination"),
        "mental_changes" => getPost("mental_changes"),
        "medications" => getPost("medications"),
        "patient_care_equipment" => getPost("patient_care_equipment"),
        "total_score" => getPost("total_score"),
        "risk_level" => getPost("risk_level")
    ];
    $id = insertTable("form_fall_risk_assessments", $form_fall_risk_assessment);
    $form_fall_risk_assessment = getTable("form_fall_risk_assessmentss", $id);
    include sendTo("back");
});

### upodate single form_fall_risk_assessment
### redirect back
$router->post("/form_fall_risk_assessments/:id", function($id) {
    $form_fall_risk_assessment = [
        "age" => getPost("age"),
        "fall_history" => getPost("fall_history"),
        "mobility" => getPost("mobility"),
        "elimination" => getPost("elimination"),
        "mental_changes" => getPost("mental_changes"),
        "medications" => getPost("medications"),
        "patient_care_equipment" => getPost("patient_care_equipment"),
        "total_score" => getPost("total_score"),
        "risk_level" => getPost("risk_level"),
        "status" => "filled"
    ];
    $id = updateTable("form_fall_risk_assessments", $form_fall_risk_assessment, $id);
    $form_fall_risk_assessment = getTable("form_fall_risk_assessments", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_fall_risk_assessments/edit/form/", function() {
    $form_fall_risk_assessment = getTable("form_fall_risk_assessments", getPost("id_form_fall_risk_assessment"));
    include linkPage("forms/form_fall_risk_assessments_edit");
});

### delete single form_fall_risk_assessment
### redirect back
$router->delete("/form_fall_risk_assessments/:id", function($id) {
    $form_fall_risk_assessment = getTable("form_fall_risk_assessments", $id);
    $id = deleteTable("form_fall_risk_assessments", $id);
    include sendTo("back");
});
