<?php

### users
### this is a generic user 
### 
### get all options with the URL base user
$router->options("/users/", function() {
    include linkPage("main/user");
});

### get single users
### return view
$router->get("/users/:id/add/day", function($id) {
    echo "start <br/>";
    foreach (getDayList() as $day):
        $data = [
            "id_user" => $id,
            "day" => $day,
            "time_start" => "0800",
            "time_end" => "1700"
        ];
        insertTable("user_availabilities", $data);
        echo $day . " done <br/>";
    endforeach;
    echo 'done <br/>';
    die();
});

### get single users
### return view
$router->get("/users/", function() {
    $users = selectTable("users");
    include linkPage("main/users/index");
});

### get single user
### return view
$router->get("/users/:id", function($id) {
    $user = getTable("users", $id);
    include linkPage("main/users/single");
});

### get single user
### return view
$router->get("/users/:id/forms", function($id) {
    $user = getTable("users", $id);
    include linkPage("main/users/forms");
});

### get single user
### return view
$router->get("/users/:id/availability", function($id) {
    $user = getTable("users", $id);
    $availabilities = selectTable("user_availabilities", ["id_user" => $id]);
    include linkPage("main/users/availability");
});

### get single user
### return view
$router->post("/users/:id/availability", function($id) {
    foreach (getPost("id") as $key => $val) :
        $data = [
            "time_start" => getPost("time_start")[$key],
            "time_end" => getPost("time_end")[$key]
        ];
        updateTable("user_availabilities", $data, getPost("id")[$key]);
    endforeach;
    sendTo("back");
});

### single single user
### redirect back
$router->post("/users/", function() {
    $user = [
        "last_name" => getPost("last_name"),
        "first_name" => getPost("first_name"),
        "middle_name" => getPost("middle_name"),
        "birthday" => getPost("birthday"),
        "sex" => getPost("sex"),
        "email" => getPost("email"),
        "mobile" => getPost("mobile"),
        "address" => getPost("address"),
        "specialization" => getPost("specialization"),
        "type" => getPost("type"),
        "password" => getPost("password")
    ];

    $id = insertTable("users", $user);
    if (getPost("type") == "doctor"):
        foreach (getDayList() as $day):
            $data = [
                "id_user" => $id,
                "day" => $day,
                "time_start" => "0800",
                "time_end" => "1700"
            ];
            insertTable("user_availabilities", $data);
        endforeach;
    endif;
    if ($_FILES["image"] != null):
        $filename = upload("image", "", "$id");
        updateTable("users", ["image" => $filename], $id);
    endif;
    $user = getTable("users", $id);
    include sendTo("back");
});

### upodate single user
### redirect back
$router->post("/users/:id", function($id) {
    $user = getTable("users", $id);
    $user = [
        "last_name" => getPost("last_name") ? $getPost("last_name") : $user["last_name"],
        "first_name" => getPost("first_name") ? $getPost("first_name") : $user["first_name"],
        "middle_name" => getPost("middle_name") ? $getPost("middle_name") : $user["middle_name"],
        "email" => getPost("email") ? $getPost("email") : $user["email"],
        "sex" => getPost("sex") ? $getPost("sex") : $user["sex"],
        "birthday" => getPost("birthday") ? $getPost("birthday") : $user["birthday"],
        "address" => getPost("address") ? $getPost("address") : $user["address"],
        "mobile" => getPost("mobile") ? $getPost("mobile") : $user["mobile"],
        "specialization" => getPost("specialization") ? $getPost("specialization") : $user["specialization"],
        "type" => getPost("type") ? $getPost("type") : $user["type"],
        "password" => getPost("password") ? $getPost("password") : $user["password"]
    ];

    $filename = upload("image", "", "$id");
    $user = getTable("users", $id);
    if ($_FILES["image"] != null):
        updateTable("users", ["image" => $filename], $id);
        updateTable("users", $user, $id);
    endif;
    include sendTo("back");
});

### delete single user
### redirect back
$router->delete("/users/:id", function($id) {
    $user = getTable("users", $id);
    $id = deleteTable("users", $id);
    include sendTo("back");
});
