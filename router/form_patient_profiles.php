<?php

### form_patient_profiles
### -> this is a generic form_patient_profile 
### 
### get all options with the URL base form_patient_profile
$router->options("/form_patient_profiles/", function() {
    include linkPage("main/form_patient_profiles/options");
});

### get single form_patient_profiles
### return view
$router->get("/form_patient_profiles/", function() {
    $form_patient_profiles = selectTable("form_patient_profiles");
    include linkPage("main/form_patient_profiles/index");
});

### add single form_patient_profiles
### return view
$router->get("/form_patient_profiles/add", function() {
    include linkPage("main/form_patient_profiles/add");
});

### get single form_patient_profile
### return view
$router->get("/form_patient_profiles/:id", function($id) {
    $form_patient_profile = getTable("form_patient_profiles", $id);
    include linkPage("main/form_patient_profiles/single");
});

### single single form_patient_profile
### redirect back
$router->post("/form_patient_profiles/", function() {
    $form_patient_profile = [
        "unit_and_room_number" => getPost("unit_and_room_number"),
        "hospital_number" => getPost("hospital_number"),
        "atending_physician" => getPost("atending_physician"),
        "occupation" => getPost("occupation"),
        "educational_attainment" => getPost("educational_attainment"),
        "nationality" => getPost("nationality"),
        "languange_or_dialect" => getPost("languange_or_dialect"),
        "informants_name" => getPost("informants_name"),
        "relationship_to_patient" => getPost("relationship_to_patient"),
        "patient_admitted_from" => getPost("patient_admitted_from"),
        "accomplished_by" => getPost("accomplished_by"),
        "accomplished_by_relationship" => getPost("accomplished_by_relationship"),
        "date_of_admission" => getPost("date_of_admission"),
        "time_of_admission" => getPost("time_of_admission"),
        "mode_of_admission" => getPost("mode_of_admission"),
        "chief_complaint" => getPost("chief_complaint"),
        "known_allergies" => getPost("known_allergies")
    ];
    $id = insertTable("form_patient_profiles", $form_patient_profile);
    $form_patient_profile = getTable("form_patient_profiless", $id);
    include sendTo("back");
});

### upodate single form_patient_profile
### redirect back
$router->post("/form_patient_profiles/:id", function($id) {
    $form_patient_profile = [
        "unit_and_room_number" => getPost("unit_and_room_number"),
        "hospital_number" => getPost("hospital_number"),
        "atending_physician" => getPost("atending_physician"),
        "occupation" => getPost("occupation"),
        "educational_attainment" => getPost("educational_attainment"),
        "nationality" => getPost("nationality"),
        "languange_or_dialect" => getPost("languange_or_dialect"),
        "informants_name" => getPost("informants_name"),
        "relationship_to_patient" => getPost("relationship_to_patient"),
        "patient_admitted_from" => getPost("patient_admitted_from"),
        "accomplished_by" => getPost("accomplished_by"),
        "accomplished_by_relationship" => getPost("accomplished_by_relationship"),
        "date_of_admission" => getPost("date_of_admission"),
        "time_of_admission" => getPost("time_of_admission"),
        "mode_of_admission" => getPost("mode_of_admission"),
        "chief_complaint" => getPost("chief_complaint"),
        "known_allergies" => getPost("known_allergies"),
        "status" => "filled"
    ];
    $id = updateTable("form_patient_profiles", $form_patient_profile, $id);
    $form_patient_profile = getTable("form_patient_profiles", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_patient_profiles/edit/form/", function() {
    $form_patient_profile = getTable("form_patient_profiles", getPost("id_form_patient_profile"));
    include linkPage("forms/form_patient_profiles_edit");
});

### delete single form_patient_profile
### redirect back
$router->delete("/form_patient_profiles/:id", function($id) {
    $form_patient_profile = getTable("form_patient_profiles", $id);
    $id = deleteTable("form_patient_profiles", $id);
    include sendTo("back");
});
