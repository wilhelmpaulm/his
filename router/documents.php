<?php

### documents
### -> this is a generic document 
### 
### get all options with the URL base document
$router->options("/documents/", function() {
    include linkPage("main/documents/options");
});

### get single documents
### return view
$router->get("/documents/", function() {
    $documents = selectTable("documents");
    include linkPage("main/documents/index");
});

### add single documents
### return view
$router->get("/documents/add", function() {
    include linkPage("main/documents/add");
});

### get single document
### return view
$router->get("/documents/:id", function($id) {
    $document = getTable("documents", $id);
    include linkPage("main/documents/single");
});

### single single document
### redirect back
$router->post("/documents/", function() {
    $document = [
        "id_patient" => getPost("id_patient"),
        "id_user" => user("id"),
        "name" => getPost("name"),
        "type" => getPost("type"),
        "description" => getPost("description"),
        "findings" => getPost("findings"),
    ];
    $id = insertTable("documents", $document);
    if ($_FILES["file"] != null):
        $filename = upload("file", "files/", "$id");
        updateTable("documents", ["file" => $filename], $id);
    endif;
    $document = getTable("documents", $id);
    include sendTo("back");
});

### upodate single document
### redirect back
$router->post("/documents/:id", function($id) {
    $document = [
        "id_user" => user("id"),
        "name" => getPost("name"),
        "type" => getPost("type"),
        "description" => getPost("description"),
        "findings" => getPost("findings"),
    ];
    updateTable("documents", $document, $id);
    if ($_FILES["file"] != null):
        $filename = upload("file", "files/", "$id");
        updateTable("documents", ["file" => $filename], $id);
    endif;
    $document = getTable("documents", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/documents/edit/form/", function() {
    $document = getTable("documents", getPost("id"));
    include linkPage("forms/documents_edit");
});

### delete single document
### redirect back
$router->get("/documents/:id/delete", function($id) {
    $document = getTable("documents", $id);
    $id = deleteTable("documents", $id);
    include sendTo("back");
});
