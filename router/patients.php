<?php

### patients
### this is a generic patient 
### 
### get all options with the URL base patient
$router->options("/patients/", function() {
    include linkPage("main/patient");
});

### registration page
$router->get('/patients/add', function() {
    include linkPage("main/patients/add");
});

### get single patients
### return view
$router->get("/patients/", function() {
    $patients = selectTable("patients");
    include linkPage("main/patients/index");
});

### get single patient
### return view
$router->get("/patients/:id", function($id) {
    $patient = getTable("patients", $id);
    include linkPage("main/patients/single");
});

### get single patient
### return view
$router->get("/patients/:id/availability", function($id) {
    $patient = getTable("patients", $id);
    include linkPage("main/patients/availability");
});

### get single patient
### return view
$router->get("/patients/:id/form_admissions/:id2", function($id, $id2) {
    $patient = getTable("patients", $id);
    $form_admission = getTable("form_admissions", $id2);
    foreach (getTableFormList("form_admissions") as $fal) :
        $$fal = getTable($fal, ["id_form_admission" => $id2]);
    endforeach;
    include linkPage("main/patients/form_admissions_index");
});

### get single patient
### return view
$router->get("/patients/:id/form_admissions/", function($id) {
    $patient = getTable("patients", $id);
    $form_admissions = selectTable("form_admissions", ["id_patient" => "$id"]);
    include linkPage("main/patients/form_admissions");
});

### get single patient
### return view
$router->get("/patients/:id/documents/", function($id) {
    $patient = getTable("patients", $id);
    $documents = selectTable("documents", ["id_patient" => "$id"]);
    include linkPage("main/patients/documents");
});

### single single patient
### redirect back
$router->post("/patients/", function() {
    $patient = [
        "last_name" => getPost("last_name"),
        "first_name" => getPost("first_name"),
        "middle_name" => getPost("middle_name"),
        "mother_last_name" => getPost("mother_last_name"),
        "mother_first_name" => getPost("mother_first_name"),
        "mother_middle_name" => getPost("mother_middle_name"),
        "father_last_name" => getPost("father_last_name"),
        "father_first_name" => getPost("father_first_name"),
        "father_middle_name" => getPost("father_middle_name"),
        "birthday" => getPost("birthday"),
        "sex" => getPost("sex"),
        "email" => getPost("email"),
        "mobile" => getPost("mobile"),
        "address" => getPost("address"),
        "company_name" => getPost("company_name"),
        "company_address" => getPost("company_address"),
        "occupation" => getPost("occupation"),
        "blood_type" => getPost("blood_type"),
        "civil_status" => getPost("civil_status"),
        "nationality" => getPost("nationality"),
        "religion" => getPost("religion"),
    ];
    $id = insertTable("patients", $patient);
    $filename = upload("image", "", "$id");
    updateTable("patients", ["image" => $filename], $id);
    $patient = getTable("patients", $id);
    include sendTo("back");
});

### upodate single patient
### redirect back
$router->post("/patients/:id", function($id) {
    $patient = getTable("patients", $id);
    $patient = [
        "last_name" => getPost("last_name") ? $getPost("last_name") : $patient["last_name"],
        "first_name" => getPost("first_name") ? $getPost("first_name") : $patient["first_name"],
        "middle_name" => getPost("mother_middle_name") ? $getPost("middle_name") : $patient["middle_name"],
        "mother_last_name" => getPost("mother_last_name") ? $getPost("mother_last_name") : $patient["mother_last_name"],
        "mother_first_name" => getPost("mother_first_name") ? $getPost("mother_first_name") : $patient["mother_first_name"],
        "mother_middle_name" => getPost("mother_middle_name") ? $getPost("mother_middle_name") : $patient["mother_middle_name"],
        "father_last_name" => getPost("father_last_name") ? $getPost("father_last_name") : $patient["father_last_name"],
        "father_first_name" => getPost("father_first_name") ? $getPost("father_first_name") : $patient["father_first_name"],
        "father_middle_name" => getPost("father_middle_name") ? $getPost("father_middle_name") : $patient["father_middle_name"],
        "birthday" => getPost("birthday") ? $getPost("birthday") : $patient["birthday"],
        "sex" => getPost("sex") ? $getPost("sex") : $patient["sex"],
        "email" => getPost("email") ? $getPost("email") : $patient["email"],
        "mobile" => getPost("mobile") ? $getPost("mobile") : $patient["mobile"],
        "address" => getPost("address") ? $getPost("address") : $patient["address"],
        "company_address" => getPost("company_address") ? $getPost("company_address") : $patient["company_address"],
        "company_name" => getPost("company_name") ? $getPost("company_name") : $patient["company_name"],
        "occupation" => getPost("occupation") ? $getPost("occupation") : $patient["occupation"],
        "blood_type" => getPost("blood_type") ? $getPost("blood_type") : $patient["blood_type"],
        "civil_status" => getPost("civil_status") ? $getPost("civil_status") : $patient["civil_status"],
        "nationality" => getPost("nationality") ? $getPost("nationality") : $patient["nationality"],
        "religion" => getPost("religion") ? $getPost("religion") : $patient["religion"],
    ];
    $filename = upload("image", "", "$id");
    $patient = getTable("patients", $id);
    updateTable("patients", ["image" => $filename], $id);
    updateTable("patients", $patient, $id);
    include sendTo("back");
});

### delete single patient
### redirect back
$router->delete("/patients/:id", function($id) {
    $patient = getTable("patients", $id);
    $id = deleteTable("patients", $id);
    include sendTo("back");
});
