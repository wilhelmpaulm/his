<?php

### domains
### -> this is a generic domain 
### 

### get all options with the URL base domain
$router->options("/domains/", function() {
    include linkPage("main/domains/options");
});

### get single domains
### return view
$router->get("/domains/", function() {
    $domains = selectTable("domains");
    include linkPage("main/domains/index");
});

### add single domains
### return view
$router->get("/domains/add", function() {
    include linkPage("main/domains/add");
});

### get single domain
### return view
$router->get("/domains/:id", function($id) {
    $domain = getTable("domains", $id);
    include linkPage("main/domains/single");
});

### single single domain
### redirect back
$router->post("/domains/", function() {
    $domain = [
        "id_user" => getUser("id"),
        "name" => getPost("name"),
        "domain" => getPost("domain"),
        "status" => "on"
    ];
    $id = insertTable("domains", $domain);
    $domain = getTable("domainss", $id);
    include sendTo("back");
});

### upodate single domain
### redirect back
$router->post("/domains/:id", function($id) {
    $domain = [
        "name" => getPost("name"),
        "domain" => getPost("domain"),
        "status" => getPost("status")
    ];
    $id = updateTable("domains", $domain, $id);
    $domain = getTable("domains", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/domains/edit/form/", function() {
    $domain = getTable("domains", getPost("id_domain"));
    include linkPage("forms/domains_edit");
});

### delete single domain
### redirect back
$router->delete("/domains/:id", function($id) {
    $domain = getTable("domains", $id);
    $id = deleteTable("domains", $id);
    include sendTo("back");
});
