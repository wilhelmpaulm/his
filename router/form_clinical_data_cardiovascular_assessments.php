<?php

### form_clinical_data_cardiovascular_assessments
### -> this is a generic form_clinical_data_cardiovascular_assessment 
### 
### get all options with the URL base form_clinical_data_cardiovascular_assessment
$router->options("/form_clinical_data_cardiovascular_assessments/", function() {
    include linkPage("main/form_clinical_data_cardiovascular_assessments/options");
});

### get single form_clinical_data_cardiovascular_assessments
### return view
$router->get("/form_clinical_data_cardiovascular_assessments/", function() {
    $form_clinical_data_cardiovascular_assessments = selectTable("form_clinical_data_cardiovascular_assessments");
    include linkPage("main/form_clinical_data_cardiovascular_assessments/index");
});

### add single form_clinical_data_cardiovascular_assessments
### return view
$router->get("/form_clinical_data_cardiovascular_assessments/add", function() {
    include linkPage("main/form_clinical_data_cardiovascular_assessments/add");
});

### get single form_clinical_data_cardiovascular_assessment
### return view
$router->get("/form_clinical_data_cardiovascular_assessments/:id", function($id) {
    $form_clinical_data_cardiovascular_assessment = getTable("form_clinical_data_cardiovascular_assessments", $id);
    include linkPage("main/form_clinical_data_cardiovascular_assessments/single");
});

### single single form_clinical_data_cardiovascular_assessment
### redirect back
$router->post("/form_clinical_data_cardiovascular_assessments/", function() {
    $form_clinical_data_cardiovascular_assessment = [
        "heart_sounds" => getPost("heart_sounds"),
        "heart_rhythm" => getPost("heart_rhythm"),
          "pulses_upper_left" => getPost("pulses_upper_left"),
        "pulses_upper_right" => getPost("pulses_upper_right"),
        "pulses_lower_left" => getPost("pulses_lower_left"),
        "pulses_lower_right" => getPost("pulses_lower_right"),
        "chest_pain" => getPost("chest_pain"),
        "chest_pain_p" => getPost("chest_pain_p"),
        "chest_pain_q" => getPost("chest_pain_q"),
        "chest_pain_r" => getPost("chest_pain_r"),
        "chest_pain_s" => getPost("chest_pain_s"),
        "chest_pain_t" => getPost("chest_pain_t"),
        "chest_pain_a" => getPost("chest_pain_a"),
    ];
    $id = insertTable("form_clinical_data_cardiovascular_assessments", $form_clinical_data_cardiovascular_assessment);
    $form_clinical_data_cardiovascular_assessment = getTable("form_clinical_data_cardiovascular_assessmentss", $id);
    include sendTo("back");
});

### upodate single form_clinical_data_cardiovascular_assessment
### redirect back
$router->post("/form_clinical_data_cardiovascular_assessments/:id", function($id) {
    $form_clinical_data_cardiovascular_assessment = [
        "heart_sounds" => getPost("heart_sounds"),
        "heart_rhythm" => getPost("heart_rhythm"),
        "pulses_upper_left" => getPost("pulses_upper_left"),
        "pulses_upper_right" => getPost("pulses_upper_right"),
        "pulses_lower_left" => getPost("pulses_lower_left"),
        "pulses_lower_right" => getPost("pulses_lower_right"),
        "chest_pain" => getPost("chest_pain"),
        "chest_pain_p" => getPost("chest_pain_p"),
        "chest_pain_q" => getPost("chest_pain_q"),
        "chest_pain_r" => getPost("chest_pain_r"),
        "chest_pain_s" => getPost("chest_pain_s"),
        "chest_pain_t" => getPost("chest_pain_t"),
        "chest_pain_a" => getPost("chest_pain_a"),
        "status" => "filled",
    ];
    $id = updateTable("form_clinical_data_cardiovascular_assessments", $form_clinical_data_cardiovascular_assessment, $id);
    $form_clinical_data_cardiovascular_assessment = getTable("form_clinical_data_cardiovascular_assessments", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_clinical_data_cardiovascular_assessments/edit/form/", function() {
    $form_clinical_data_cardiovascular_assessment = getTable("form_clinical_data_cardiovascular_assessments", getPost("id_form_clinical_data_cardiovascular_assessment"));
    include linkPage("forms/form_clinical_data_cardiovascular_assessments_edit");
});

### delete single form_clinical_data_cardiovascular_assessment
### redirect back
$router->delete("/form_clinical_data_cardiovascular_assessments/:id", function($id) {
    $form_clinical_data_cardiovascular_assessment = getTable("form_clinical_data_cardiovascular_assessments", $id);
    $id = deleteTable("form_clinical_data_cardiovascular_assessments", $id);
    include sendTo("back");
});
