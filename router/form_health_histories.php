<?php

### form_health_histories
### -> this is a generic form_health_history 
### 
### get all options with the URL base form_health_history
$router->options("/form_health_histories/", function() {
    include linkPage("main/form_health_histories/options");
});

### get single form_health_histories
### return view
$router->get("/form_health_histories/", function() {
    $form_health_histories = selectTable("form_health_histories");
    include linkPage("main/form_health_histories/index");
});

### add single form_health_histories
### return view
$router->get("/form_health_histories/add", function() {
    include linkPage("main/form_health_histories/add");
});

### get single form_health_history
### return view
$router->get("/form_health_histories/:id", function($id) {
    $form_health_history = getTable("form_health_histories", $id);
    include linkPage("main/form_health_histories/single");
});

### single single form_health_history
### redirect back
$router->post("/form_health_histories/", function() {
    $form_health_history = [
        "previous_hospitalization" => getPost("previous_hospitalization"),
        "procedures_undertaken" => getPost("procedures_undertaken")
    ];
    $id = insertTable("form_health_histories", $form_health_history);
    $form_health_history = getTable("form_health_historiess", $id);
    include sendTo("back");
});

### upodate single form_health_history
### redirect back
$router->post("/form_health_histories/:id", function($id) {
    $form_health_history = [
        "previous_hospitalization" => getPost("previous_hospitalization"),
        "procedures_undertaken" => getPost("procedures_undertaken"),
        "status" => "filled"
    ];
    $id = updateTable("form_health_histories", $form_health_history, $id);
    $form_health_history = getTable("form_health_histories", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_health_histories/edit/form/", function() {
    $form_health_history = getTable("form_health_histories", getPost("id_form_health_history"));
    include linkPage("forms/form_health_histories_edit");
});

### delete single form_health_history
### redirect back
$router->delete("/form_health_histories/:id", function($id) {
    $form_health_history = getTable("form_health_histories", $id);
    $id = deleteTable("form_health_histories", $id);
    include sendTo("back");
});
