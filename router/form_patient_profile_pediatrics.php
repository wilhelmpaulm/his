<?php

### form_patient_profile_pediatrics
### -> this is a generic form_patient_profile_pediatric 
### 
### get all options with the URL base form_patient_profile_pediatric
$router->options("/form_patient_profile_pediatrics/", function() {
    include linkPage("main/form_patient_profile_pediatrics/options");
});

### get single form_patient_profile_pediatrics
### return view
$router->get("/form_patient_profile_pediatrics/", function() {
    $form_patient_profile_pediatrics = selectTable("form_patient_profile_pediatrics");
    include linkPage("main/form_patient_profile_pediatrics/index");
});

### add single form_patient_profile_pediatrics
### return view
$router->get("/form_patient_profile_pediatrics/add", function() {
    include linkPage("main/form_patient_profile_pediatrics/add");
});

### get single form_patient_profile_pediatric
### return view
$router->get("/form_patient_profile_pediatrics/:id", function($id) {
    $form_patient_profile_pediatric = getTable("form_patient_profile_pediatrics", $id);
    include linkPage("main/form_patient_profile_pediatrics/single");
});

### single single form_patient_profile_pediatric
### redirect back
$router->post("/form_patient_profile_pediatrics/", function() {
    $form_patient_profile_pediatric = [
        "baptized" => getPost("baptized"),
        "child_nickname" => getPost("child_nickname"),
        "siblings_number" => getPost("siblings_number"),
        "guardian" => getPost("guardian"),
        "most_significant_person_to_patient" => getPost("most_significant_person_to_patient"),
        "neonatal_history" => getPost("neonatal_history"),
        "delivery" => getPost("delivery"),
        "condition_at_birth" => getPost("condition_at_birth"),
        "associated_anomalies" => getPost("associated_anomalies")
    ];
    $id = insertTable("form_patient_profile_pediatrics", $form_patient_profile_pediatric);
    $form_patient_profile_pediatric = getTable("form_patient_profile_pediatricss", $id);
    include sendTo("back");
});

### upodate single form_patient_profile_pediatric
### redirect back
$router->post("/form_patient_profile_pediatrics/:id", function($id) {
    $form_patient_profile_pediatric = [
        "baptized" => getPost("baptized"),
        "child_nickname" => getPost("child_nickname"),
        "siblings_number" => getPost("siblings_number"),
        "guardian" => getPost("guardian"),
        "most_significant_person_to_patient" => getPost("most_significant_person_to_patient"),
        "neonatal_history" => getPost("neonatal_history"),
        "delivery" => getPost("delivery"),
        "condition_at_birth" => getPost("condition_at_birth"),
        "associated_anomalies" => getPost("associated_anomalies"),
        "status" => "filled"
    ];
    $id = updateTable("form_patient_profile_pediatrics", $form_patient_profile_pediatric, $id);
    $form_patient_profile_pediatric = getTable("form_patient_profile_pediatrics", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_patient_profile_pediatrics/edit/form/", function() {
    $form_patient_profile_pediatric = getTable("form_patient_profile_pediatrics", getPost("id_form_patient_profile_pediatric"));
    include linkPage("forms/form_patient_profile_pediatrics_edit");
});

### delete single form_patient_profile_pediatric
### redirect back
$router->delete("/form_patient_profile_pediatrics/:id", function($id) {
    $form_patient_profile_pediatric = getTable("form_patient_profile_pediatrics", $id);
    $id = deleteTable("form_patient_profile_pediatrics", $id);
    include sendTo("back");
});
