<?php

### form_health_history_daily_living_habits
### -> this is a generic form_health_history_daily_living_habit 
### 
### get all options with the URL base form_health_history_daily_living_habit
$router->options("/form_health_history_daily_living_habits/", function() {
    include linkPage("main/form_health_history_daily_living_habits/options");
});

### get single form_health_history_daily_living_habits
### return view
$router->get("/form_health_history_daily_living_habits/", function() {
    $form_health_history_daily_living_habits = selectTable("form_health_history_daily_living_habits");
    include linkPage("main/form_health_history_daily_living_habits/index");
});

### add single form_health_history_daily_living_habits
### return view
$router->get("/form_health_history_daily_living_habits/add", function() {
    include linkPage("main/form_health_history_daily_living_habits/add");
});

### get single form_health_history_daily_living_habit
### return view
$router->get("/form_health_history_daily_living_habits/:id", function($id) {
    $form_health_history_daily_living_habit = getTable("form_health_history_daily_living_habits", $id);
    include linkPage("main/form_health_history_daily_living_habits/single");
});

### single single form_health_history_daily_living_habit
### redirect back
$router->post("/form_health_history_daily_living_habits/", function() {
    $form_health_history_daily_living_habit = [
        "nutrition" => getPost("nutrition"),
        "exercise" => getPost("exercise"),
        "alcohol" => getPost("alcohol"),
        "smoking" => getPost("smoking"),
        "sleeping_pattern" => getPost("sleeping_pattern")
    ];
    $id = insertTable("form_health_history_daily_living_habits", $form_health_history_daily_living_habit);
    $form_health_history_daily_living_habit = getTable("form_health_history_daily_living_habitss", $id);
    include sendTo("back");
});

### upodate single form_health_history_daily_living_habit
### redirect back
$router->post("/form_health_history_daily_living_habits/:id", function($id) {
    $form_health_history_daily_living_habit = [
        "nutrition" => getPost("nutrition"),
        "exercise" => getPost("exercise"),
        "alcohol" => getPost("alcohol"),
        "smoking" => getPost("smoking"),
        "sleeping_pattern" => getPost("sleeping_pattern"),
        "status" => "filled"
    ];
    $id = updateTable("form_health_history_daily_living_habits", $form_health_history_daily_living_habit, $id);
    $form_health_history_daily_living_habit = getTable("form_health_history_daily_living_habits", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_health_history_daily_living_habits/edit/form/", function() {
    $form_health_history_daily_living_habit = getTable("form_health_history_daily_living_habits", getPost("id_form_health_history_daily_living_habit"));
    include linkPage("forms/form_health_history_daily_living_habits_edit");
});

### delete single form_health_history_daily_living_habit
### redirect back
$router->delete("/form_health_history_daily_living_habits/:id", function($id) {
    $form_health_history_daily_living_habit = getTable("form_health_history_daily_living_habits", $id);
    $id = deleteTable("form_health_history_daily_living_habits", $id);
    include sendTo("back");
});
