<?php

### form_clinical_data_neurological_assessments
### -> this is a generic form_clinical_data_neurological_assessment 
### 
### get all options with the URL base form_clinical_data_neurological_assessment
$router->options("/form_clinical_data_neurological_assessments/", function() {
    include linkPage("main/form_clinical_data_neurological_assessments/options");
});

### get single form_clinical_data_neurological_assessments
### return view
$router->get("/form_clinical_data_neurological_assessments/", function() {
    $form_clinical_data_neurological_assessments = selectTable("form_clinical_data_neurological_assessments");
    include linkPage("main/form_clinical_data_neurological_assessments/index");
});

### add single form_clinical_data_neurological_assessments
### return view
$router->get("/form_clinical_data_neurological_assessments/add", function() {
    include linkPage("main/form_clinical_data_neurological_assessments/add");
});

### get single form_clinical_data_neurological_assessment
### return view
$router->get("/form_clinical_data_neurological_assessments/:id", function($id) {
    $form_clinical_data_neurological_assessment = getTable("form_clinical_data_neurological_assessments", $id);
    include linkPage("main/form_clinical_data_neurological_assessments/single");
});

### single single form_clinical_data_neurological_assessment
### redirect back
$router->post("/form_clinical_data_neurological_assessments/", function() {
    $form_clinical_data_neurological_assessment = [
        "oriented_to" => getPost("oriented_to"),
        "headache" => getPost("headache"),
        "headache_how_long" => getPost("headache_how_long"),
        "headache_when" => getPost("headache_when"),
        "loss_of_consciousness" => getPost("loss_of_consciousness"),
        "loss_of_consciousness_how_long" => getPost("loss_of_consciousness_how_long"),
        "loss_of_consciousness_when" => getPost("loss_of_consciousness_when"),
        "seizure_episode" => getPost("seizure_episode"),
        "seizure_episode_when" => getPost("seizure_episode_when"),
        "seizure_episode_how_long" => getPost("seizure_episode_how_long"),
        "pupils_left" => getPost("pupils_left"),
        "pupils_right_status" => getPost("pupils_right"),
        "pupils_left_status" => getPost("pupils_left_status_status"),
        "pupils_right" => getPost("pupils_right"),
        "coma_scale_eyes_open" => getPost("coma_scale_eyes_open"),
        "coma_scale_best_verbal_response" => getPost("coma_scale_best_verbal_response"),
        "coma_scale_best_motor_response" => getPost("coma_scale_best_motor_response"),
        "coma_scale_total_score" => getPost("coma_scale_total_score")
    ];
    $id = insertTable("form_clinical_data_neurological_assessments", $form_clinical_data_neurological_assessment);
    $form_clinical_data_neurological_assessment = getTable("form_clinical_data_neurological_assessmentss", $id);
    include sendTo("back");
});

### upodate single form_clinical_data_neurological_assessment
### redirect back
$router->post("/form_clinical_data_neurological_assessments/:id", function($id) {
    $form_clinical_data_neurological_assessment = [
        "oriented_to" => getPost("oriented_to"),
         "headache" => getPost("headache"),
        "headache_how_long" => getPost("headache_how_long"),
        "headache_when" => getPost("headache_when"),
        "loss_of_consciousness" => getPost("loss_of_consciousness"),
        "loss_of_consciousness_how_long" => getPost("loss_of_consciousness_how_long"),
        "loss_of_consciousness_when" => getPost("loss_of_consciousness_when"),
        "seizure_episode" => getPost("seizure_episode"),
        "seizure_episode_when" => getPost("seizure_episode_when"),
        "seizure_episode_how_long" => getPost("seizure_episode_how_long"),
        "pupils_left" => getPost("pupils_left"),
        "pupils_right_status" => getPost("pupils_right"),
        "pupils_left_status" => getPost("pupils_left_status_status"),
        "pupils_right" => getPost("pupils_right"),
        "coma_scale_eyes_open" => getPost("coma_scale_eyes_open"),
        "coma_scale_best_verbal_response" => getPost("coma_scale_best_verbal_response"),
        "coma_scale_best_motor_response" => getPost("coma_scale_best_motor_response"),
        "coma_scale_total_score" => getPost("coma_scale_total_score"),
        "status" => "filled"
    ];
    $id = updateTable("form_clinical_data_neurological_assessments", $form_clinical_data_neurological_assessment, $id);
    $form_clinical_data_neurological_assessment = getTable("form_clinical_data_neurological_assessments", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_clinical_data_neurological_assessments/edit/form/", function() {
    $form_clinical_data_neurological_assessment = getTable("form_clinical_data_neurological_assessments", getPost("id_form_clinical_data_neurological_assessment"));
    include linkPage("forms/form_clinical_data_neurological_assessments_edit");
});

### delete single form_clinical_data_neurological_assessment
### redirect back
$router->delete("/form_clinical_data_neurological_assessments/:id", function($id) {
    $form_clinical_data_neurological_assessment = getTable("form_clinical_data_neurological_assessments", $id);
    $id = deleteTable("form_clinical_data_neurological_assessments", $id);
    include sendTo("back");
});
