<?php

### form_health_history_obgyn_histories
### -> this is a generic form_health_history_obgyn_history 
### 
### get all options with the URL base form_health_history_obgyn_history
$router->options("/form_health_history_obgyn_histories/", function() {
    include linkPage("main/form_health_history_obgyn_histories/options");
});

### get single form_health_history_obgyn_histories
### return view
$router->get("/form_health_history_obgyn_histories/", function() {
    $form_health_history_obgyn_histories = selectTable("form_health_history_obgyn_histories");
    include linkPage("main/form_health_history_obgyn_histories/index");
});

### add single form_health_history_obgyn_histories
### return view
$router->get("/form_health_history_obgyn_histories/add", function() {
    include linkPage("main/form_health_history_obgyn_histories/add");
});

### get single form_health_history_obgyn_history
### return view
$router->get("/form_health_history_obgyn_histories/:id", function($id) {
    $form_health_history_obgyn_history = getTable("form_health_history_obgyn_histories", $id);
    include linkPage("main/form_health_history_obgyn_histories/single");
});

### single single form_health_history_obgyn_history
### redirect back
$router->post("/form_health_history_obgyn_histories/", function() {
    $form_health_history_obgyn_history = [
        "menarche_age" => getPost("menarche_age"),
        "lmp" => getPost("lmp"),
        "parity" => getPost("parity"),
        "gravida" => getPost("gravida"),
        "tpal_preterm" => getPost("tpal_preterm"),
        "tpal_abortion" => getPost("tpal_abortion"),
        "tpal_live_birth" => getPost("tpal_live_birth"),
        "contraceptive_type" => getPost("contraceptive_type")
    ];
    $id = insertTable("form_health_history_obgyn_histories", $form_health_history_obgyn_history);
    $form_health_history_obgyn_history = getTable("form_health_history_obgyn_historiess", $id);
    include sendTo("back");
});

### upodate single form_health_history_obgyn_history
### redirect back
$router->post("/form_health_history_obgyn_histories/:id", function($id) {
    $form_health_history_obgyn_history = [
        "menarche_age" => getPost("menarche_age"),
        "lmp" => getPost("lmp"),
        "parity" => getPost("parity"),
        "gravida" => getPost("gravida"),
        "tpal_preterm" => getPost("tpal_preterm"),
        "tpal_abortion" => getPost("tpal_abortion"),
        "tpal_live_birth" => getPost("tpal_live_birth"),
        "contraceptive_type" => getPost("contraceptive_type"),
        "status" => "filled"
    ];
    $id = updateTable("form_health_history_obgyn_histories", $form_health_history_obgyn_history, $id);
    $form_health_history_obgyn_history = getTable("form_health_history_obgyn_histories", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_health_history_obgyn_histories/edit/form/", function() {
    $form_health_history_obgyn_history = getTable("form_health_history_obgyn_histories", getPost("id_form_health_history_obgyn_history"));
    include linkPage("forms/form_health_history_obgyn_histories_edit");
});

### delete single form_health_history_obgyn_history
### redirect back
$router->delete("/form_health_history_obgyn_histories/:id", function($id) {
    $form_health_history_obgyn_history = getTable("form_health_history_obgyn_histories", $id);
    $id = deleteTable("form_health_history_obgyn_histories", $id);
    include sendTo("back");
});
