<?php

### form_clinical_data_respiratory_assessments
### -> this is a generic form_clinical_data_respiratory_assessment 
### 
### get all options with the URL base form_clinical_data_respiratory_assessment
$router->options("/form_clinical_data_respiratory_assessments/", function() {
    include linkPage("main/form_clinical_data_respiratory_assessments/options");
});

### get single form_clinical_data_respiratory_assessments
### return view
$router->get("/form_clinical_data_respiratory_assessments/", function() {
    $form_clinical_data_respiratory_assessments = selectTable("form_clinical_data_respiratory_assessments");
    include linkPage("main/form_clinical_data_respiratory_assessments/index");
});

### add single form_clinical_data_respiratory_assessments
### return view
$router->get("/form_clinical_data_respiratory_assessments/add", function() {
    include linkPage("main/form_clinical_data_respiratory_assessments/add");
});

### get single form_clinical_data_respiratory_assessment
### return view
$router->get("/form_clinical_data_respiratory_assessments/:id", function($id) {
    $form_clinical_data_respiratory_assessment = getTable("form_clinical_data_respiratory_assessments", $id);
    include linkPage("main/form_clinical_data_respiratory_assessments/single");
});

### single single form_clinical_data_respiratory_assessment
### redirect back
$router->post("/form_clinical_data_respiratory_assessments/", function() {
    $form_clinical_data_respiratory_assessment = [
        "breathing_pattern" => getPost("breathing_pattern"),
        "breath_sounds" => getPost("breath_sounds"),
        "chest_movement" => getPost("chest_movement"),
        "oxygenation" => getPost("oxygenation"),
    ];
    $id = insertTable("form_clinical_data_respiratory_assessments", $form_clinical_data_respiratory_assessment);
    $form_clinical_data_respiratory_assessment = getTable("form_clinical_data_respiratory_assessmentss", $id);
    include sendTo("back");
});

### upodate single form_clinical_data_respiratory_assessment
### redirect back
$router->post("/form_clinical_data_respiratory_assessments/:id", function($id) {
    $form_clinical_data_respiratory_assessment = [
        "breathing_pattern" => getPost("breathing_pattern"),
        "breath_sounds" => getPost("breath_sounds"),
        "chest_movement" => getPost("chest_movement"),
        "oxygenation" => getPost("oxygenation"),
        "status" => "filled"
    ];
    $id = updateTable("form_clinical_data_respiratory_assessments", $form_clinical_data_respiratory_assessment, $id);
    $form_clinical_data_respiratory_assessment = getTable("form_clinical_data_respiratory_assessments", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_clinical_data_respiratory_assessments/edit/form/", function() {
    $form_clinical_data_respiratory_assessment = getTable("form_clinical_data_respiratory_assessments", getPost("id_form_clinical_data_respiratory_assessment"));
    include linkPage("forms/form_clinical_data_respiratory_assessments_edit");
});

### delete single form_clinical_data_respiratory_assessment
### redirect back
$router->delete("/form_clinical_data_respiratory_assessments/:id", function($id) {
    $form_clinical_data_respiratory_assessment = getTable("form_clinical_data_respiratory_assessments", $id);
    $id = deleteTable("form_clinical_data_respiratory_assessments", $id);
    include sendTo("back");
});
