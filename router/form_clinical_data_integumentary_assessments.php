<?php

### form_clinical_data_integumentary_assessments
### -> this is a generic form_clinical_data_integumentary_assessment 
### 
### get all options with the URL base form_clinical_data_integumentary_assessment
$router->options("/form_clinical_data_integumentary_assessments/", function() {
    include linkPage("main/form_clinical_data_integumentary_assessments/options");
});

### get single form_clinical_data_integumentary_assessments
### return view
$router->get("/form_clinical_data_integumentary_assessments/", function() {
    $form_clinical_data_integumentary_assessments = selectTable("form_clinical_data_integumentary_assessments");
    include linkPage("main/form_clinical_data_integumentary_assessments/index");
});

### add single form_clinical_data_integumentary_assessments
### return view
$router->get("/form_clinical_data_integumentary_assessments/add", function() {
    include linkPage("main/form_clinical_data_integumentary_assessments/add");
});

### get single form_clinical_data_integumentary_assessment
### return view
$router->get("/form_clinical_data_integumentary_assessments/:id", function($id) {
    $form_clinical_data_integumentary_assessment = getTable("form_clinical_data_integumentary_assessments", $id);
    include linkPage("main/form_clinical_data_integumentary_assessments/single");
});

### single single form_clinical_data_integumentary_assessment
### redirect back
$router->post("/form_clinical_data_integumentary_assessments/", function() {
    $form_clinical_data_integumentary_assessment = [
        "skin_integrity" => getPost("skin_integrity"),
        "color" => getPost("color"),
        "texture" => getPost("texture"),
        "moisture" => getPost("moisture"),
        "turgor" => getPost("turgor"),
        "edema" => getPost("edema"),
        "edema_site" => getPost("edema_site"),
        "others" => getPost("others"),
        "iv_access" => getPost("iv_access"),
        "iv_access_status" => getPost("iv_access_status"),
        "iv_access_location" => getPost("iv_access_location"),
        "presence_of_pressure" => getPost("presence_of_pressure"),
        "areas" => getPost("areas"),
        "measurement_length" => getPost("measurement_length"),
        "measurement_width" => getPost("measurement_width"),
        "stage_of_ulcer" => getPost("stage_of_ulcer"),
        "exudates" => getPost("exudates"),
        "wound_pain" => getPost("wound_pain"),
        "wound_pain_location" => getPost("wound_pain_location"),
        "wound_pain_scale" => getPost("wound_pain_scale")
    ];
    $id = insertTable("form_clinical_data_integumentary_assessments", $form_clinical_data_integumentary_assessment);
    $form_clinical_data_integumentary_assessment = getTable("form_clinical_data_integumentary_assessmentss", $id);
    include sendTo("back");
});

### upodate single form_clinical_data_integumentary_assessment
### redirect back
$router->post("/form_clinical_data_integumentary_assessments/:id", function($id) {
    $form_clinical_data_integumentary_assessment = [
        "skin_integrity" => getPost("skin_integrity"),
        "color" => getPost("color"),
        "texture" => getPost("texture"),
        "moisture" => getPost("moisture"),
        "turgor" => getPost("turgor"),
        "edema" => getPost("edema"),
        "edema_site" => getPost("edema_site"),
        "others" => getPost("others"),
        "iv_access" => getPost("iv_access"),
        "iv_access_status" => getPost("iv_access_status"),
        "iv_access_location" => getPost("iv_access_location"),
        "presence_of_pressure" => getPost("presence_of_pressure"),
        "areas" => getPost("areas"),
        "measurement_length" => getPost("measurement_length"),
        "measurement_width" => getPost("measurement_width"),
        "stage_of_ulcer" => getPost("stage_of_ulcer"),
        "exudates" => getPost("exudates"),
        "wound_pain" => getPost("wound_pain"),
        "wound_pain_location" => getPost("wound_pain_location"),
        "wound_pain_scale" => getPost("wound_pain_scale"),
        "status" => "filled",
    ];
    $id = updateTable("form_clinical_data_integumentary_assessments", $form_clinical_data_integumentary_assessment, $id);
    $form_clinical_data_integumentary_assessment = getTable("form_clinical_data_integumentary_assessments", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_clinical_data_integumentary_assessments/edit/form/", function() {
    $form_clinical_data_integumentary_assessment = getTable("form_clinical_data_integumentary_assessments", getPost("id_form_clinical_data_integumentary_assessment"));
    include linkPage("forms/form_clinical_data_integumentary_assessments_edit");
});

### delete single form_clinical_data_integumentary_assessment
### redirect back
$router->delete("/form_clinical_data_integumentary_assessments/:id", function($id) {
    $form_clinical_data_integumentary_assessment = getTable("form_clinical_data_integumentary_assessments", $id);
    $id = deleteTable("form_clinical_data_integumentary_assessments", $id);
    include sendTo("back");
});
