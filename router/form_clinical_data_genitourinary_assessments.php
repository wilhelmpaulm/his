<?php

### form_clinical_data_genitourinary_assessments
### -> this is a generic form_clinical_data_genitourinary_assessment 
### 
### get all options with the URL base form_clinical_data_genitourinary_assessment
$router->options("/form_clinical_data_genitourinary_assessments/", function() {
    include linkPage("main/form_clinical_data_genitourinary_assessments/options");
});

### get single form_clinical_data_genitourinary_assessments
### return view
$router->get("/form_clinical_data_genitourinary_assessments/", function() {
    $form_clinical_data_genitourinary_assessments = selectTable("form_clinical_data_genitourinary_assessments");
    include linkPage("main/form_clinical_data_genitourinary_assessments/index");
});

### add single form_clinical_data_genitourinary_assessments
### return view
$router->get("/form_clinical_data_genitourinary_assessments/add", function() {
    include linkPage("main/form_clinical_data_genitourinary_assessments/add");
});

### get single form_clinical_data_genitourinary_assessment
### return view
$router->get("/form_clinical_data_genitourinary_assessments/:id", function($id) {
    $form_clinical_data_genitourinary_assessment = getTable("form_clinical_data_genitourinary_assessments", $id);
    include linkPage("main/form_clinical_data_genitourinary_assessments/single");
});

### single single form_clinical_data_genitourinary_assessment
### redirect back
$router->post("/form_clinical_data_genitourinary_assessments/", function() {
    $form_clinical_data_genitourinary_assessment = [
        "urinary_pattern" => getPost("urinary_pattern"),
        "genital_area" => getPost("genital_area"),
        "genitourinary_pain" => getPost("genitourinary_pain"),
        "genitourinary_pain_location" => getPost("genitourinary_pain_location"),
        "genitourinary_pain_scale" => getPost("genitourinary_pain_scale"),
        "contraptions" => getPost("contraptions")
    ];
    $id = insertTable("form_clinical_data_genitourinary_assessments", $form_clinical_data_genitourinary_assessment);
    $form_clinical_data_genitourinary_assessment = getTable("form_clinical_data_genitourinary_assessmentss", $id);
    include sendTo("back");
});

### upodate single form_clinical_data_genitourinary_assessment
### redirect back
$router->post("/form_clinical_data_genitourinary_assessments/:id", function($id) {
    $form_clinical_data_genitourinary_assessment = [
        "urinary_pattern" => getPost("urinary_pattern"),
        "genital_area" => getPost("genital_area"),
        "genitourinary_pain" => getPost("genitourinary_pain"),
        "genitourinary_pain_location" => getPost("genitourinary_pain_location"),
        "genitourinary_pain_scale" => getPost("genitourinary_pain_scale"),
        "contraptions" => getPost("contraptions"),
        "status" => "filled"
    ];
    $id = updateTable("form_clinical_data_genitourinary_assessments", $form_clinical_data_genitourinary_assessment, $id);
    $form_clinical_data_genitourinary_assessment = getTable("form_clinical_data_genitourinary_assessments", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_clinical_data_genitourinary_assessments/edit/form/", function() {
    $form_clinical_data_genitourinary_assessment = getTable("form_clinical_data_genitourinary_assessments", getPost("id_form_clinical_data_genitourinary_assessment"));
    include linkPage("forms/form_clinical_data_genitourinary_assessments_edit");
});

### delete single form_clinical_data_genitourinary_assessment
### redirect back
$router->delete("/form_clinical_data_genitourinary_assessments/:id", function($id) {
    $form_clinical_data_genitourinary_assessment = getTable("form_clinical_data_genitourinary_assessments", $id);
    $id = deleteTable("form_clinical_data_genitourinary_assessments", $id);
    include sendTo("back");
});
