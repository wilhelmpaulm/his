<?php

### form_clinical_data_musculoskeletal_assessments
### -> this is a generic form_clinical_data_musculoskeletal_assessment 
### 
### get all options with the URL base form_clinical_data_musculoskeletal_assessment
$router->options("/form_clinical_data_musculoskeletal_assessments/", function() {
    include linkPage("main/form_clinical_data_musculoskeletal_assessments/options");
});

### get single form_clinical_data_musculoskeletal_assessments
### return view
$router->get("/form_clinical_data_musculoskeletal_assessments/", function() {
    $form_clinical_data_musculoskeletal_assessments = selectTable("form_clinical_data_musculoskeletal_assessments");
    include linkPage("main/form_clinical_data_musculoskeletal_assessments/index");
});

### add single form_clinical_data_musculoskeletal_assessments
### return view
$router->get("/form_clinical_data_musculoskeletal_assessments/add", function() {
    include linkPage("main/form_clinical_data_musculoskeletal_assessments/add");
});

### get single form_clinical_data_musculoskeletal_assessment
### return view
$router->get("/form_clinical_data_musculoskeletal_assessments/:id", function($id) {
    $form_clinical_data_musculoskeletal_assessment = getTable("form_clinical_data_musculoskeletal_assessments", $id);
    include linkPage("main/form_clinical_data_musculoskeletal_assessments/single");
});

### single single form_clinical_data_musculoskeletal_assessment
### redirect back
$router->post("/form_clinical_data_musculoskeletal_assessments/", function() {
    $form_clinical_data_musculoskeletal_assessment = [
        "gate" => getPost("gate"),
        "rom" => getPost("rom"),
        "musculoskeletall_pain" => getPost("musculoskeletall_pain"),
        "musculoskeletall_pain_location" => getPost("musculoskeletall_pain_location"),
        "musculoskeletall_pain_scale" => getPost("musculoskeletall_pain_scale"),
        "mobility_aides" => getPost("mobility_aides")
    ];
    $id = insertTable("form_clinical_data_musculoskeletal_assessments", $form_clinical_data_musculoskeletal_assessment);
    $form_clinical_data_musculoskeletal_assessment = getTable("form_clinical_data_musculoskeletal_assessmentss", $id);
    include sendTo("back");
});

### upodate single form_clinical_data_musculoskeletal_assessment
### redirect back
$router->post("/form_clinical_data_musculoskeletal_assessments/:id", function($id) {
    $form_clinical_data_musculoskeletal_assessment = [
        "gate" => getPost("gate"),
        "rom" => getPost("rom"),
        "musculoskeletall_pain" => getPost("musculoskeletall_pain"),
        "musculoskeletall_pain_location" => getPost("musculoskeletall_pain_location"),
        "musculoskeletall_pain_scale" => getPost("musculoskeletall_pain_scale"),
        "mobility_aides" => getPost("mobility_aides"),
        "status" => "filled"
    ];
    $id = updateTable("form_clinical_data_musculoskeletal_assessments", $form_clinical_data_musculoskeletal_assessment, $id);
    $form_clinical_data_musculoskeletal_assessment = getTable("form_clinical_data_musculoskeletal_assessments", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_clinical_data_musculoskeletal_assessments/edit/form/", function() {
    $form_clinical_data_musculoskeletal_assessment = getTable("form_clinical_data_musculoskeletal_assessments", getPost("id_form_clinical_data_musculoskeletal_assessment"));
    include linkPage("forms/form_clinical_data_musculoskeletal_assessments_edit");
});

### delete single form_clinical_data_musculoskeletal_assessment
### redirect back
$router->delete("/form_clinical_data_musculoskeletal_assessments/:id", function($id) {
    $form_clinical_data_musculoskeletal_assessment = getTable("form_clinical_data_musculoskeletal_assessments", $id);
    $id = deleteTable("form_clinical_data_musculoskeletal_assessments", $id);
    include sendTo("back");
});
