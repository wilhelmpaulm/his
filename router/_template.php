<?php

### templates
### -> this is a generic template 
### 

### get all options with the URL base template
$router->options("/templates/", function() {
    include linkPage("main/template");
});

### get single templates
### return view
$router->get("/templates/", function() {
    $templates = selectTable("templates");
    include linkPage("main/template");
});

### get single template
### return view
$router->get("/template/:id", function($id) {
    $template = getTable("templates", $id);
    include linkPage("main/template");
});

### single single template
### redirect back
$router->post("/templates/", function() {
    $template = [];
    $id = insertTable("templates", $template);
    $template = getTable("templates", $id);
    include sendTo("back");
});

### upodate single template
### redirect back
$router->post("/templates/:id", function($id) {
    $template = [];
    $id = updateTable("templates", $template, $id);
    $template = getTable("templates", $id);
    include sendTo("back");
});

### delete single template
### redirect back
$router->delete("/templates/:id", function($id) {
    $template = getTable("templates", $id);
    $id = deleteTable("templates", $id);
    include sendTo("back");
});
