<?php

### form_health_history_past_medical_histories
### -> this is a generic form_health_history_past_medical_history 
### 
### get all options with the URL base form_health_history_past_medical_history
$router->options("/form_health_history_past_medical_histories/", function() {
    include linkPage("main/form_health_history_past_medical_histories/options");
});

### get single form_health_history_past_medical_histories
### return view
$router->get("/form_health_history_past_medical_histories/", function() {
    $form_health_history_past_medical_histories = selectTable("form_health_history_past_medical_histories");
    include linkPage("main/form_health_history_past_medical_histories/index");
});

### add single form_health_history_past_medical_histories
### return view
$router->get("/form_health_history_past_medical_histories/add", function() {
    include linkPage("main/form_health_history_past_medical_histories/add");
});

### get single form_health_history_past_medical_history
### return view
$router->get("/form_health_history_past_medical_histories/:id", function($id) {
    $form_health_history_past_medical_history = getTable("form_health_history_past_medical_histories", $id);
    include linkPage("main/form_health_history_past_medical_histories/single");
});

### single single form_health_history_past_medical_history
### redirect back
$router->post("/form_health_history_past_medical_histories/", function() {
    $form_health_history_past_medical_history = [
        "past_medical_history" => getPost("past_medical_history"),
        "immunizations" => getPost("immunizations")
    ];
    $id = insertTable("form_health_history_past_medical_histories", $form_health_history_past_medical_history);
    $form_health_history_past_medical_history = getTable("form_health_history_past_medical_historiess", $id);
    include sendTo("back");
});

### upodate single form_health_history_past_medical_history
### redirect back
$router->post("/form_health_history_past_medical_histories/:id", function($id) {
    $form_health_history_past_medical_history = [
        "past_medical_history" => getPost("past_medical_history"),
        "immunizations" => getPost("immunizations"),
        "status" => "filled"
    ];
    $id = updateTable("form_health_history_past_medical_histories", $form_health_history_past_medical_history, $id);
    $form_health_history_past_medical_history = getTable("form_health_history_past_medical_histories", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_health_history_past_medical_histories/edit/form/", function() {
    $form_health_history_past_medical_history = getTable("form_health_history_past_medical_histories", getPost("id_form_health_history_past_medical_history"));
    include linkPage("forms/form_health_history_past_medical_histories_edit");
});

### delete single form_health_history_past_medical_history
### redirect back
$router->delete("/form_health_history_past_medical_histories/:id", function($id) {
    $form_health_history_past_medical_history = getTable("form_health_history_past_medical_histories", $id);
    $id = deleteTable("form_health_history_past_medical_histories", $id);
    include sendTo("back");
});
