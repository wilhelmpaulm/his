<?php

### form_hospital_orientations
### -> this is a generic form_hospital_orientation 
### 
### get all options with the URL base form_hospital_orientation
$router->options("/form_hospital_orientations/", function() {
    include linkPage("main/form_hospital_orientations/options");
});

### get single form_hospital_orientations
### return view
$router->get("/form_hospital_orientations/", function() {
    $form_hospital_orientations = selectTable("form_hospital_orientations");
    include linkPage("main/form_hospital_orientations/index");
});

### add single form_hospital_orientations
### return view
$router->get("/form_hospital_orientations/add", function() {
    include linkPage("main/form_hospital_orientations/add");
});

### get single form_hospital_orientation
### return view
$router->get("/form_hospital_orientations/:id", function($id) {
    $form_hospital_orientation = getTable("form_hospital_orientations", $id);
    include linkPage("main/form_hospital_orientations/single");
});

### single single form_hospital_orientation
### redirect back
$router->post("/form_hospital_orientations/", function() {
    $form_hospital_orientation = [
        "orientation_given_to" => getPost("orientation_given_to"),
        "orientation_to" => getPost("orientation_to")
    ];
    $id = insertTable("form_hospital_orientations", $form_hospital_orientation);
    $form_hospital_orientation = getTable("form_hospital_orientationss", $id);
    include sendTo("back");
});

### upodate single form_hospital_orientation
### redirect back
$router->post("/form_hospital_orientations/:id", function($id) {
    $form_hospital_orientation = [
        "orientation_given_to" => getPost("orientation_given_to"),
        "orientation_to" => getPost("orientation_to"),
        "status" => "filled"
    ];
    $id = updateTable("form_hospital_orientations", $form_hospital_orientation, $id);
    $form_hospital_orientation = getTable("form_hospital_orientations", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/form_hospital_orientations/edit/form/", function() {
    $form_hospital_orientation = getTable("form_hospital_orientations", getPost("id_form_hospital_orientation"));
    include linkPage("forms/form_hospital_orientations_edit");
});

### delete single form_hospital_orientation
### redirect back
$router->delete("/form_hospital_orientations/:id", function($id) {
    $form_hospital_orientation = getTable("form_hospital_orientations", $id);
    $id = deleteTable("form_hospital_orientations", $id);
    include sendTo("back");
});
