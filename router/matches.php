<?php

### matches
### -> this is a generic matche 
### 
### get all options with the URL base matche
$router->options("/matches/", function() {
    include linkPage("main/matches/options");
});



### add single matches
### return view
$router->get("/matches/add", function() {
    include linkPage("main/matches/add");
});

### get single matche
### return view
$router->get("/matches/:id", function($id) {
    $matche = getTable("matches", $id);
    include linkPage("main/matches/single");
});

### single single matche
### redirect back
$router->get("/matches/", function() {
   
    $id_patient = getGet("id_patient");
    $patient = getTable("patients",getGet("id_patient"));
    $doctors_old = selectTable("users", ["type" => "doctor"]);
    $doctors = [];
    $query = trim(urldecode(getGet("query")));
    $specializations = [];
    if ($query != null) :
        foreach (getSpecializationOptionList() as $key => $val) :
            $text = str_replace(" ", "|", $query);
            $text = str_replace(",", "|", $text);
            $text = str_replace("-", "|", $text);
//            $reg = "/";
            $reg = "/\b(";
            $text = explode("|", $text);
            foreach ($text as $key => $t) :
//                $reg .= '(?=.*?' . $t . ")";
                $reg .= $t;
                if ($key < count($text) - 1):
                    $reg .= "|";
                endif;
            endforeach;

//            $reg .= "^.*$/i";
            $reg .= ")\b/i";
            $pool = strtolower(implode(" ", $val["list"]));

            if (preg_match($reg, $pool)) :
                array_push($specializations, $val["specialization"]);
            endif;
        endforeach;
    endif;

    foreach ($specializations as $spe):
        foreach ($doctors_old as $doc):
            if ($spe == $doc["specialization"] && !in_array($doc, $doctors)):
                array_push($doctors, $doc);
            endif;
        endforeach;
    endforeach;

    $users = $doctors;
    include linkPage("main/matches/index");
});

### upodate single matche
### redirect back
$router->post("/matches/:id", function($id) {
    $matche = [
        "name" => getPost("name"),
        "matche" => getPost("matche"),
        "status" => getPost("status")
    ];
    $id = updateTable("matches", $matche, $id);
    $matche = getTable("matches", $id);
    include sendTo("back");
});

### upodate single key
### redirect back
$router->post("/matches/add/form/", function() {
    $patient = getTable("patients", getPost("id"));
    include linkPage("forms/matches_add");
});

### delete single matche
### redirect back
$router->delete("/matches/:id", function($id) {
    $matche = getTable("matches", $id);
    $id = deleteTable("matches", $id);
    include sendTo("back");
});
